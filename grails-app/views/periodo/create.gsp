<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'periodo.label', default: 'Periodo')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<main class="container mb15 mt25">
    <section class="">
        <div class="row">
            <g:link controller="periodo" action="index" class="btn ">Voltar para lista</g:link>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="col"></div>

            <div class="col s12 m12 l12">
                <div class="card white darken-1">
                <div class="card-content black-text">
                    <span class="card-title center">Cadastrar Periodo</span>
                    <g:form controller="periodo" action="save">
                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="Periodo do dia" id="nome_id" type="text" name="nome"
                                       class="validate">
                                <label for="nome_id">Nome</label>
                            </div>

                            <div class="input-field col s3">
                                <input id="inicio_id" type="text" name="inicio" class="timepicker">
                                <label for="inicio_id">Inicio</label>
                            </div>

                            <div class="input-field col s3">
                                <input id="fim_id" type="text" name="fim" class="timepicker">
                                <label for="fim_id">Fim</label>
                            </div>
                        </div>

                        </div>

                        <div class="card-action">
                            <button class="btn btn-block" type="submit">Cadastrar</button>
                        </div>
                    </g:form>
                </div>
            </div>

            <div class="col"></div>
        </div>
    </section>
</main>

</body>
</html>
