// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require jquery-3.3.1.js
//= require material_js/materialize.js
//= require hpm_food/retornaCategoriaPeriodo.js
//= require hpm_food/retornaItemRefeicao.js
//= require hpm_food/retornaRefeicaoPeriodo.js
//= require hpm_food/retornaDadosFuncionario.js

if (typeof jQuery !== 'undefined') {
    (function ($) {
        $(document).ajaxStart(function () {
            $('#spinner').fadeIn();
        }).ajaxStop(function () {
            $('#spinner').fadeOut();
        });
    })(jQuery);
}

$(document).ready(function(){
    $('.tabs').tabs();
});

$(document).ready(function () {
    $('#encontre').eq(0).addClass('active');
    $('#encontre2').eq(0).addClass('active');

    $('.collapsible').collapsible();

    $(".dropdown-trigger").dropdown({
        hover: false
    });

    $('.fixed-action-btn').floatingActionButton();

    $('.sidenav').sidenav();

    $('.datepicker').datepicker({
        i18n: {
            months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabádo'],
            weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
            weekdaysAbbrev: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
            today: 'Hoje',
            clear: 'Limpar',
            cancel: 'Sair',
            done: 'Confirmar',
            labelMonthNext: 'Próximo mês',
            labelMonthPrev: 'Mês anterior',
            labelMonthSelect: 'Selecione um mês',
            labelYearSelect: 'Selecione um ano',
            selectMonths: true,
            selectYears: 15,
        },
        format: 'dd/mm/yyyy ',
        container: 'body',
        minDate: new Date(),
    });

    $('.timepicker').timepicker({
        twelveHour: false
    });

    $('select').formSelect();

});

$(document).ready(function () {
    $('.modal').modal();
});

$(document).ready(function () {
    $('.tap-target').tapTarget();
});

var MAX_CARBOIDRATO = 2;
var MAX_LANCHE = 3;
var MAX = 1;

$(function () {
    $(".carboidratoCheck").click(function () {
        if ($(".carboidratoCheck :checked").length > MAX_CARBOIDRATO) {
            M.toast({html: 'Limite de Carboidratos atingido!'})
            event.preventDefault();
        }
    });
});

$(function () {
    $(".proteinaVegetalCheck").click(function () {
        if ($(".proteinaVegetalCheck :checked").length > MAX) {
            M.toast({html: 'Limite de Proteina Vegetal atingido!'})
            event.preventDefault();
        }
    });
});

$(function () {
    $(".proteinaAnimalCheck").click(function () {
        if ($(".proteinaAnimalCheck :checked").length > MAX) {
            M.toast({html: 'Limite de Proteinas Animal atingido!'})
            event.preventDefault();
        }
    });
});

$(function () {
    $(".adicionalCheck").click(function () {
        if ($(".adicionalCheck :checked").length > MAX) {
            M.toast({html: 'Limite de Adicionais atingido!'})
            event.preventDefault();
        }
    });
});

$(function () {
    $(".lancheCheck").click(function () {
        if ($(".lancheCheck :checked").length > MAX_LANCHE) {
            M.toast({html: 'Limite de Lanches atingido!'})
            event.preventDefault();
        }
    });
});

$(function () {
    $(".guarnicaoCheck").click(function () {
        if ($(".guarnicaoCheck :checked").length > MAX) {
            M.toast({html: 'Limite de Guarnições atingido!'})
            event.preventDefault();
        }
    });
});

$(function () {
    $(".saladaCheck").click(function () {
        if ($(".saladaCheck :checked").length > MAX) {
            M.toast({html: 'Limite de Saladas atingido!'})
            event.preventDefault();
        }
    });
});

$(function () {
    $(".bebidaCheck").click(function () {
        if ($(".bebidaCheck :checked").length > MAX) {
            M.toast({html: 'Limite de Bebidas atingido!'})
            event.preventDefault();
        }
    });
});



