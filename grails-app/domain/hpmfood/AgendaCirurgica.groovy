package hpmfood

import grails.databinding.BindingFormat

class AgendaCirurgica {

    Long id
    Pessoa paciente
    Pessoa medico
    @BindingFormat('dd/MM/yyyy HH:mm')
    Date data_agenda
    Procedimento procedimento
    Long cod_atendimento
    String nome_sala
    String nome_paciente_provisorio
    Convenio convenio
    Long estabelecimento


    static constraints = {
    }

    static mapping = {
        version false
        table name: 'MV_AGENDA_CIRURGICA'
        id column: 'NR_SEQUENCIA'
        paciente column: 'CD_PESSOA_FISICA'
        medico column: 'CD_MEDICO'
        data_agenda column: 'DT_AGENDA', sqlType: Date
        procedimento column: 'NR_SEQ_PROC_INTERNO'
        cod_atendimento column: 'NR_ATENDIMENTO'
        nome_sala column: 'DS_AGENDA'
        nome_paciente_provisorio column: 'NM_PACIENTE'
        convenio column: 'CD_CONVENIO'
        estabelecimento column: 'CD_ESTABELECIMENTO'
    }

}

