package hpmfood

class RegistroRefeicaoFunc {

    Date entradaAlmoco
    Date saidaAlmoco

    static belongsTo = [funcionario: Pessoa]

    static constraints = {
        funcionario nullable: false
        entradaAlmoco nullable: false
        saidaAlmoco nullable: true
    }
}
