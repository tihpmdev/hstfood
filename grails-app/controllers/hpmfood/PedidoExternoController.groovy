package hpmfood

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PedidoExternoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    RegrasDataService regrasDataService
    PedidoExternoService pedidoExternoService

    def index() {
        Boolean isExcluido = true
        List<PedidoExterno> pedidosExterno = PedidoExterno.executeQuery('''from PedidoExterno 
                                                                        where exclusaoLogica != :isExcluido 
                                                                        order by solicitacao desc''', [isExcluido: isExcluido])
        respond pedidos: pedidosExterno
    }

    def show(PedidoExterno pedidoExterno) {
        respond pedidoExterno
    }

    def create() {

        Boolean isExcluido = true
        List<Refeicao> refeicoes = Refeicao.findAll('''from Refeicao order by nome''')
        List<Dieta> dietas = Dieta.findAll('''from Dieta order by nome''')
        List<Setor> locais = Setor.findAll('''from Setor order by nome''')
        List<Patologia> patologias = Patologia.findAll('''from Patologia where isExcluido != :excluido order by nome''',[ excluido: isExcluido])
        respond(new PedidoExterno(params), model: [refeicoes: refeicoes, dietas: dietas, locais: locais, patologia: patologias])
    }

    def dashBoardPedidoExterno() {

        Status cancelado = Status.findById(49)
        Status finalizado = Status.findById(50)
        Boolean isExcluido = true

        List<PedidoExterno> pedidosExterno = PedidoExterno.withCriteria {
            ne('exclusaoLogica', isExcluido)
            ne('status', cancelado)
            and {
                ne('status', finalizado)
            }
            order('solicitacao', 'desc')
        } as List<PedidoExterno>

        respond pedidos: pedidosExterno

    }

    def relPedidosExterno() {

        def (List setores, List refeicoes, List statusList, List<PedidoExterno> pedidos) = pedidoExternoService.retornaConsultaPedidoPaciente(params)

        respond(setores: setores, refeicoes: refeicoes, status: statusList, pedidos: pedidos)

    }

    def historicoExterno() {
        List<PedidoExterno> pedidosExterno = PedidoExterno.executeQuery('''from PedidoExterno order by solicitacao desc''')
        respond pedidos: pedidosExterno
    }

    def iniciarPreparo(Long id) {

        PedidoExterno pedido = PedidoExterno.get(id)
        pedido.status = Status.findById(36)
        pedido.save flush: true, failOnError: true
        redirect(action: 'dashBoardPedidoExterno')
    }

    def encaminharPedidoEstabelecimento(Long id) {

        PedidoExterno pedido = PedidoExterno.get(id)
        pedido.status = Status.findById(47)
        pedido.encaminhamento = new Date()

        pedido.save flush: true, failOnError: true
        redirect(action: 'dashBoardPedidoExterno')
    }

    def entregarPedidoEstabelecimento(Long id) {

        PedidoExterno pedido = PedidoExterno.get(id)
        pedido.status = Status.findById(48)
        pedido.entrega = new Date()

        pedido.save flush: true, failOnError: true
        redirect(action: 'dashBoardPedidoExterno')
    }

    def reabrirPedido(Long id) {

        PedidoExterno pedido = PedidoExterno.get(id)
        pedido.status = Status.findById(22)
        pedido.save flush: true, failOnError: true
        redirect(action: 'dashBoardPedidoExterno')
    }

    def cancelarPedido(Long id) {

        PedidoExterno pedido = PedidoExterno.get(id)
        pedido.status = Status.findById(49)
        pedido.cancelamento = new Date()
        pedido.save flush: true, failOnError: true
        redirect(action: 'dashBoardPedidoExterno')
    }

    def zerarStatus(Long id) {

        PedidoExterno pedido = PedidoExterno.get(id)
        pedido.status = Status.findById(22)
        pedido.save flush: true, failOnError: true
        redirect(action: 'dashBoardPedidoExterno')
    }

    def finalizarPedido(Long id) {
        PedidoExterno pedido = PedidoExterno.get(id)
        pedido.status = Status.findById(50)
        pedido.finalizar = new Date()
        pedido.save flush: true, failOnError: true
        redirect(action: 'dashBoardPedidoExterno')
    }

    @Transactional
    def save(def params) {

        PedidoExterno pedido = new PedidoExterno()
        List<Patologia> patologiasCheck = params.list('patologia')


        if (!params?.acompanhante) {
            params?.acompanhante = false
        }

        Date dataParams
        String dataFormatada

        if (!params.pedidoData || !params.pedidoHora) {
            dataParams = new Date()
        } else {
            dataFormatada = params.pedidoData + params.pedidoHora
            dataParams = new Date().parse('dd/MM/yyyy HH:mm', dataFormatada)
        }

        pedido.solicitante = Pessoa.findById(params.solicitante)
        pedido.refeicao = Refeicao.findById(params.refeicao)
        pedido.dieta = Dieta.findById(params.dieta)
        pedido.setor = Setor.findById(params.setor)
        pedido.status = Status.findById(22)
        pedido.paciente = params.paciente
        pedido.temAcompanhante = params.acompanhante
        pedido.observacao = params.observacao
        pedido.solicitacao = new Date()
        pedido.pedidoEntrega = dataParams
        pedido.exclusaoLogica = false

        for (def patologia : patologiasCheck) {
            Patologia patologiaCheck = Patologia.findById(patologia)
            pedido.addToPatologia(patologiaCheck)
            pedido.save flush: true, failOnError: true
            flash.message = "Seu pedido foi efetuado com sucesso!"
        }

        redirect(action: 'dashBoardPedidoExterno')
    }

    def edit(PedidoExterno pedidoExterno) {
        respond pedidoExterno
    }

    @Transactional
    def update(PedidoExterno pedidoExterno) {
        if (pedidoExterno == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (pedidoExterno.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond pedidoExterno.errors, view: 'edit'
            return
        }

        pedidoExterno.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'pedidoExterno.label', default: 'PedidoExterno'), pedidoExterno.id])
                redirect pedidoExterno
            }
            '*' { respond pedidoExterno, [status: OK] }
        }
    }

    def excluirPedido(Long id){

        PedidoExterno pedidoExt = PedidoExterno.get(id)
        pedidoExt.exclusaoLogica = true
        pedidoExt.save flush: true, failOnError: true
        flash.message = "Pedido ${pedidoExt.id} excluido."
        redirect(action: 'dashBoardPedidoExterno')
    }

    @Transactional
    def delete(PedidoExterno pedidoExterno) {

        if (pedidoExterno == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        pedidoExterno.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'pedidoExterno.label', default: 'PedidoExterno'), pedidoExterno.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'pedidoExterno.label', default: 'PedidoExterno'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
