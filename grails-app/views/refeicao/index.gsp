<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'refeicao.label', default: 'Refeicao')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>

    <main class="container mb15 mt25">
        <section class="mb1rem mt1rem">
            <table class="striped">
                <thead>
                <tr class="">
                    <th>Nome</th>
                    <th>Periodo</th>
                    <th>Item</th>
                    <th>Excluído?</th>
                </tr>
                </thead>
                <tbody class="">
                <g:each in="${refeicaoList}" var="per">
                    <tr>
                        <td>${per.nome}</td>
                        <td>${per.periodo}</td>
                        <td>${per.item}</td>
                        <td>${per.exclusaoLogica}</td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </section>
        <section class="mt25 mb15">
            <g:link controller="refeicao" action="create" class="btn">Criar Refeição</g:link>
        </section>
    </main>

    </body>
</html>