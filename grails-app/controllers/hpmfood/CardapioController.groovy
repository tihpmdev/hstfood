package hpmfood

import grails.converters.JSON

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CardapioController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    CategoriaService categoriaService
    CardapioService cardapioService
    RegrasDataService regrasDataService

    def index() {
        if (session?.usuario) {

            Pessoa medicoLogado = Pessoa.findById(session?.usuario?.idPessoa)
            Medico medico = Medico.findByPessoa(medicoLogado)
            Status pedidoFinalizado = Status.findById(50)
            Boolean isExcluido = true

            List<Cardapio> itensCardapio = Cardapio.executeQuery('''from Cardapio where medico= :medico 
                                                                         and status != :pedidoFinalizado
                                                                         and exclusaoLogica != :isExcluido 
                                                                         order by pedido desc''', [medico: medico, pedidoFinalizado: pedidoFinalizado, isExcluido: isExcluido])

            respond(itemCardapio: itensCardapio)
        } else {
            redirect(controller: 'autenticacao', action: 'acesso')
        }

    }

    def retornaMedicoAPI() {
        List<Medico> medicos = Medico.executeQuery('''select pessoa.nome from Medico order by pessoa.nome asc''')
        render(medicos as JSON)
    }

    def relTempoProduzido() {}

    def relMedicoPedido() {}

    def relPedidos(def params) {
        if (session?.usuario) {


            def (List setores, List refeicoes, List statusList, List<Cardapio> cardapios) = cardapioService.retornaConsultaPedidoInterno(params)

            respond(setores: setores, refeicoes: refeicoes, status: statusList, cardapios: cardapios)

        } else {
            redirect(action: 'logout', controller: 'autenticacao')
        }
    }

    def cardapioMedicoDiario() {
        if (session?.usuario) {
            Pessoa medicoLogado = Pessoa.findById(session?.usuario?.idPessoa)
            Medico medico = Medico.findByPessoa(medicoLogado)
            Status pedidoFinalizado = Status.findById(50)

            Date dataAtualPedido = new Date()
            dataAtualPedido.clearTime()
            Boolean isExcluido = true

            List<Cardapio> cardapios = Cardapio.withCriteria {
                eq('medico', medico)
                and {
                    ge('pedido', dataAtualPedido)
                    ne('status', pedidoFinalizado)
                    ne('exclusaoLogica', isExcluido)
                }
            } as List<Cardapio>

            respond(cardapios: cardapios)
        } else {
            redirect(view: 'acesso', controller: 'autenticacao')
        }
    }

    def cardapioDashboard() {

        if (session?.usuario) {
            retornaPedidosNaoFinalizados()
        } else {
            redirect(controller: 'autenticacao', action: 'acesso')
        }

    }

    private void retornaPedidosNaoFinalizados() {

        Status pedidoFinalizado = Status.findById(50)
        Boolean isExcluido = true
        List<Cardapio> cardapios = Cardapio.executeQuery('''from Cardapio where status != :pedidoFinalizado
                                                                and exclusaoLogica != :isExcluido 
                                                                order by pedido desc''', [pedidoFinalizado: pedidoFinalizado, isExcluido: isExcluido])
        respond(itemCardapio: cardapios)
    }

    def cardapioDashboardRecente() {

        if (session?.usuario) {
            Status pedidoFinalizado = Status.findById(50)
            Date dataAtualClear = new Date()
            Date dataAtualFull = new Date()
            Calendar dataUltimaHora = regrasDataService.retornaUltimaHoraDia()
            List<Cardapio> cardapios
            Boolean isExcluido = true

            cardapios = Cardapio.withCriteria {
                ne('status', pedidoFinalizado)
                ne('exclusaoLogica', isExcluido)
                and {
                    between('pedido', dataAtualClear.clearTime(), dataUltimaHora.getTime())
                }

            } as List<Cardapio>

            respond(itemCardapio: cardapios)
        } else {
            redirect(controller: 'autenticacao', action: 'acesso')
        }

    }

    def refeicaoPorPeriodo() {

        def list = cardapioService.listRefeicaoPorPeriodo(params.long('periodoId'))
        render(list as JSON)
    }

    def historicoPedidoCompleto() {
        if (session?.usuario) {
            Boolean isExcluido = true
            List<Cardapio> cardapios = Cardapio.executeQuery('''from Cardapio
                                                               where exclusaoLogica != :isExcluido
                                                               order by pedido desc''', [isExcluido: isExcluido])
            respond(itemCardapio: cardapios)
        } else {
            redirect(controller: 'autenticacao', action: 'acesso')
        }
    }

    def show(Cardapio cardapio) {

        List<RegistroConversa> registros = RegistroConversa.executeQuery('''from RegistroConversa where cardapio= :cardapio order by envio desc''', [cardapio: cardapio])
        respond(cardapio, model: [registros: registros])

    }

    def create() {

        if (session?.usuario) {
            List<Refeicao> listRefeicao = Refeicao.findAll("from Refeicao order by nome")
            List<Periodo> listPeriodo = Periodo.findAll("from Periodo order by nome")
            List<Setor> listaSetorMedico = Setor.executeQuery('''from Setor where id in(202, 203, 204,205, 206) order by nome''')

            def (List<Item> itemAdicional,
            List<Item>      itemLanche,
            List<Item>      itemCarboidrato,
            List<Item>      itemGuarnicao,
            List<Item>      itemProteinaAnimal,
            List<Item>      itemProteinaVegetal,
            List<Item>      itemSalada,
            List<Item>      itemBebida) = categoriaService.retornaItensPorCategoria()

            respond(new Cardapio(params), model: [periodos           : listPeriodo,
                                                  refeicoes          : listRefeicao,
                                                  itemAdicional      : itemAdicional,
                                                  itemLanche         : itemLanche,
                                                  itemCarboidrato    : itemCarboidrato,
                                                  itemGuarnicao      : itemGuarnicao,
                                                  itemProteinaAnimal : itemProteinaAnimal,
                                                  itemProteinaVegetal: itemProteinaVegetal,
                                                  itemSalada         : itemSalada,
                                                  itemBebida         : itemBebida,
                                                  setores            : listaSetorMedico])
        } else {
            redirect(controller: 'autenticacao', action: 'acesso')
        }
    }

    def iniciarPreparo(Long id) {

        Cardapio cardapio = Cardapio.get(id)
        cardapio.status = Status.findById(36)
        cardapio.preparo = new Date()

        cardapio.save flush: true, failOnError: true
        redirect(action: 'cardapioDashboard')
    }

    def encaminharPedidoEstabelecimento(Long id) {

        Cardapio cardapio = Cardapio.get(id)
        cardapio.status = Status.findById(47)
        cardapio.encaminhamento = new Date()

        cardapio.save flush: true, failOnError: true
        redirect(action: 'cardapioDashboard')
    }

    def entregarPedidoEstabelecimento(Long id) {

        Cardapio cardapio = Cardapio.get(id)
        cardapio.status = Status.findById(48)
        cardapio.entrega = new Date()

        cardapio.save flush: true, failOnError: true
        redirect(action: 'cardapioDashboard')
    }

    def reabrirPedido(Long id) {

        Cardapio cardapio = Cardapio.get(id)
        cardapio.status = Status.findById(22)
        cardapio.save flush: true, failOnError: true
        redirect(action: 'cardapioDashboard')
    }

    def cancelarPedido(Long id) {

        Cardapio cardapio = Cardapio.get(id)
        cardapio.status = Status.findById(49)
        cardapio.save flush: true, failOnError: true
        redirect(action: 'cardapioDashboard')
//        retornaCancelamentoPedido(params)
//        flash.message = "O pedido ${params.cardapioId} foi cancelado."
        redirect(action: 'cardapioDashboard')
    }

    def cancelarPedidoMedico(Long id) {

        Cardapio cardapio = Cardapio.get(id)
        cardapio.status = Status.findById(49)
        cardapio.save flush: true, failOnError: true
//        retornaCancelamentoPedido(params)
        redirect(action: 'index', controller: 'autenticacao')
    }

    void retornaCancelamentoPedido(params) {
        Cardapio cardapio = Cardapio.get(params.cardapioId)
        cardapio.status = Status.findById(params.status)
        cardapio.cancelamento = new Date()

//        LogStatus logsCardapio = LogStatus.findByCardapio(cardapio)
//
//        if (!logsCardapio) {
//            LogStatus logStatus = new LogStatus()
//            logStatus.descricao = params.motivoCancelamento
//            logStatus.pessoa = Pessoa.findById(params?.usuario)
//            logStatus.insercao = new Date()
//            logStatus.status = Status.findById(params.status)
//            logStatus.cardapio = Cardapio.findById(cardapio.id)
//            logStatus.save(flush: true, failOnError: true)

//            cardapio.addToLogStatus(logStatus)
//        }
        cardapio.save flush: true, failOnError: true

    }

    def zerarStatus(Long id) {

        Cardapio cardapio = Cardapio.get(id)
        cardapio.status = Status.findById(22)
        cardapio.save flush: true, failOnError: true
        redirect(action: 'cardapioDashboard')
    }

    def finalizarPedido(Long id) {
        Cardapio cardapio = Cardapio.get(id)
        cardapio.status = Status.findById(50)
        cardapio.finalizar = new Date()
        cardapio.save flush: true, failOnError: true
        redirect(action: 'cardapioDashboard')
    }

    @Transactional
    def save(def params) {

        Cardapio cardapio = new Cardapio()
        List<Cardapio> cardapios
        Boolean isPedidoInvalido = false

        Pessoa medicoLogado = Pessoa.findById(session?.usuario?.idPessoa)
        Medico medico = Medico.findByPessoa(medicoLogado)
        Status cancelado = Status.findById(49)
        Status finalizado = Status.findById(50)

        Date dataAtualClear = new Date()
        Calendar dataUltimaHora = regrasDataService.retornaUltimaHoraDia()
        Date dataLimiteAlmoco = regrasDataService.retornaHoraLimiteAlmoco()
        Date dataLimiteJantar = regrasDataService.retornaHoraLimiteJantar()

        cardapios = Cardapio.withCriteria {
            eq('medico', medico)
            ne('status', cancelado)
            or {
                ne('status', finalizado)
            }
            and {
                between('solicitacao', dataAtualClear.clearTime(), dataUltimaHora.getTime())
            }
            and{
                eq('exclusaoLogica', false)
            }
        } as List<Cardapio>


        List<Item> itensCheck = params.list('item')
        Date dataParams
        Date dataRegraLimite
        Date solicitacao = new Date()
        Date paramsClearDate
        String dataFormatada

        if (!params.pedidoData || !params.pedidoHora) {
            dataParams = new Date()
        } else {
            dataFormatada = params.pedidoData + params.pedidoHora
            dataParams = new Date().parse('dd/MM/yyyy HH:mm', dataFormatada)
            dataRegraLimite = new Date().parse('dd/MM/yyyy HH:mm', dataFormatada)
        }


        Refeicao refeicaoParametro = Refeicao.findById(params.refeicao)
        Refeicao almoco = Refeicao.findById(4)
        Refeicao jantar = Refeicao.findById(16)
        paramsClearDate = dataRegraLimite


        if (paramsClearDate.clearTime() == dataAtualClear.clearTime()) {
            if (refeicaoParametro == almoco) {
                if (solicitacao > dataLimiteAlmoco) {
                    println('Limite horario almoco')
                    isPedidoInvalido = true
                }
            }
            if (refeicaoParametro == jantar) {
                if (solicitacao > dataLimiteJantar) {
                    println('Limite horario jantar')
                    isPedidoInvalido = true
                }
            }
        }

        for (def cardap : cardapios) {
            if (cardap.refeicao == refeicaoParametro) {
                if (paramsClearDate.clearTime() == dataAtualClear.clearTime())
                    isPedidoInvalido = true
            }
        }

        if (!isPedidoInvalido) {
            cardapio.pedido = dataParams
            cardapio.observacao = params?.observacao
            cardapio.exclusaoLogica = false
            cardapio.solicitacao = solicitacao
            cardapio.setor = Setor.findById(params.setor)
            cardapio.refeicao = Refeicao.findById(params.refeicao)
            cardapio.periodo = Periodo.findById(params.periodo)
            cardapio.status = Status.findById(params.status)
            cardapio.medico = Medico.findByPessoa(medicoLogado)

            for (def itemCheck : itensCheck) {
                Item item = Item.findById(itemCheck)
                cardapio.addToItem(item)
                cardapio.save flush: true, failOnError: true
                flash.message = "Seu pedido foi efetuado com sucesso!"
            }

            def assunto = "HPMFood - Hospital Palmas Medical"
            sendMail {
                to "nutricao@hospitalpalmasmedical.com.br"
                cc "tihpmdev@gmail.com", "rayssapimentanutri@gmail.com"
                subject assunto
                html view: 'corpoEmail', model: [
                        solicitante   : cardapio?.medico?.pessoa?.nome,
                        itemSolicitado: cardapio?.item,
                        solicitacao   : cardapio?.solicitacao,
                        entregaPedido : cardapio?.pedido,
                        observacao    : cardapio?.observacao,
                        codigoPedido  : cardapio?.id
                ]
            }

            redirect(action: 'cardapioMedicoDiario', controller: 'cardapio')
        } else {
            flash.message = "Não foi possivel realizar a solicitação, verifique o horário limite de pedidos da refeição no rodapé, ou se ja solicitou essa mesma refeição hoje."
            redirect(action: 'create')
        }


    }

    def excluirPedido(Long id) {

        Cardapio cardapio = Cardapio.get(id)
        cardapio.exclusaoLogica = true
        cardapio.save flush: true, failOnError: true
        flash.message = "Pedido ${cardapio.id} excluido."
        redirect(action: 'cardapioDashboard')
    }

    def edit(Cardapio cardapio) {
        respond cardapio
    }

    @Transactional
    def update(Cardapio cardapio) {
        if (cardapio == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (cardapio.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond cardapio.errors, view: 'edit'
            return
        }

        cardapio.save flush: true

        request.withFormat {

            '*' { respond cardapio, [status: OK] }
        }
    }

    @Transactional
    def delete(Cardapio cardapio) {

        if (cardapio == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        cardapio.delete flush: true

        request.withFormat {

            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {

            '*' { render status: NOT_FOUND }
        }
    }
}
