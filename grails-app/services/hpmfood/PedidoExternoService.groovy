package hpmfood

import grails.transaction.Transactional

@Transactional
class PedidoExternoService {

    List retornaConsultaPedidoPaciente(params) {

        List<Setor> setores = Setor.executeQuery('''from Setor order by nome asc''')
        List<Refeicao> refeicoes = Refeicao.executeQuery('''from Refeicao order by nome asc''')
        List<Status> statusList = Status.executeQuery('''from Status order by nome asc''')
        List<PedidoExterno> pedidos = []

        Setor setorParams = null
        Refeicao refeicaoParams = null
        Status statusParams = null
        String horaInicial = " 00:00"
        String horaFinal = " 23:59"
        Date dataParamsEntregaInicial = null
        Date dataParamsEntregaFinal = null


        if (params?.buscaSetor) {
            setorParams = Setor.findById(params.buscaSetor)
        }
        if (params?.buscaRefeicao) {
            refeicaoParams = Refeicao.findById(params.buscaRefeicao)
        }
        if (params?.buscaStatus) {
            statusParams = Status.findById(params.buscaStatus)
        }
        if (params.buscaDataEntrega) {
            dataParamsEntregaInicial = new Date().parse('dd/MM/yyyy HH:mm', params.buscaDataEntrega + horaInicial)
            dataParamsEntregaFinal = new Date().parse('dd/MM/yyyy HH:mm', params.buscaDataEntrega + horaFinal)
        }

        if (params?.buscaRefeicao != null || params?.buscaSetor != null || params?.buscaStatus != null) {
            pedidos = PedidoExterno.withCriteria {
                eq('exclusaoLogica', false)
                if (statusParams) {
                    like('status', statusParams)
                }
                if (setorParams) {
                    and { like('setor', setorParams) }
                }
                if (refeicaoParams) {
                    and { like('refeicao', refeicaoParams) }
                }
                if (dataParamsEntregaInicial) {
                    and { between('pedidoEntrega', dataParamsEntregaInicial, dataParamsEntregaFinal) }
                }
                order('pedidoEntrega', 'desc')

            } as List<PedidoExterno>
        }
        [setores, refeicoes, statusList, pedidos]
    }

}
