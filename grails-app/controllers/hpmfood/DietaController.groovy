package hpmfood

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class DietaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Dieta.list(params), model:[dietaCount: Dieta.count()]
    }

    def show(Dieta dieta) {
        respond dieta
    }

    def create() {
        respond new Dieta(params)
    }

    @Transactional
    def save(Dieta dieta) {
        if (dieta == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (dieta.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond dieta.errors, view:'create'
            return
        }

        dieta.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'dieta.label', default: 'Dieta'), dieta.id])
                redirect dieta
            }
            '*' { respond dieta, [status: CREATED] }
        }
    }

    def edit(Dieta dieta) {
        respond dieta
    }

    @Transactional
    def update(Dieta dieta) {
        if (dieta == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (dieta.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond dieta.errors, view:'edit'
            return
        }

        dieta.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'dieta.label', default: 'Dieta'), dieta.id])
                redirect dieta
            }
            '*'{ respond dieta, [status: OK] }
        }
    }

    @Transactional
    def delete(Dieta dieta) {

        if (dieta == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        dieta.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'dieta.label', default: 'Dieta'), dieta.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'dieta.label', default: 'Dieta'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
