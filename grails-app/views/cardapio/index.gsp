<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'cardapio.label', default: 'Cardapio')}"/>
    <title>Cardapio Médico</title>
</head>

<body>

<main class="mt25 container">
    <g:if test="${itemCardapio != []}">
        <section class="container row">

            <h5 class="center-align mt2rem">Painel de pedidos</h5>

            <div class="container col s12 m12 l12 infoBackground">
                <span class="col s12 m12 l12 font-normal center-align">Ordenados em recentes solicitados</span>
            </div>
        </section>
        <section>
            <div class="">
                <g:each in="${itemCardapio}" var="cardapioMedico">
                    <div class="col s12 m12 l4">

                    <g:if test="${cardapioMedico?.status?.nome == "Novo Pedido"}">
                        <div class="card statusNovoPedido">
                    <div class="card-content">
                        <div class="card-top">
                        <span class="new badge blue" data-badge-caption="${cardapioMedico?.status?.nome}">
                    </g:if>
                    <g:elseif test="${cardapioMedico?.status?.nome == "Em preparo"}">
                        <div class="card statusEmPreparo">
                        <div class="card-content">
                       <div class="card-top">
                       <span class="new badge purple" data-badge-caption="${cardapioMedico?.status?.nome}">
                    </g:elseif>
                    <g:elseif test="${cardapioMedico?.status?.nome == "Saiu para entrega"}">
                        <div class="card statusEmMovimento">
                        <div class="card-content ">
                       <div class="card-top">
                       <span class="new badge orange" data-badge-caption="${cardapioMedico?.status?.nome}">
                    </g:elseif>
                    <g:elseif test="${cardapioMedico?.status?.nome == "Entregue"}">
                        <div class="card statusEntregue">
                        <div class="card-content ">
                       <div class="card-top">
                       <span class="new badge green" data-badge-caption="${cardapioMedico?.status?.nome}">
                    </g:elseif>
                    <g:elseif test="${cardapioMedico?.status?.nome == "Cancelado"}">
                        <div class="card statusCancelado">
                        <div class="card-content ">
                       <div class="card-top">
                       <span class="new badge red" data-badge-caption="${cardapioMedico?.status?.nome}">
                    </g:elseif>
                    <g:elseif test="${cardapioMedico?.status?.nome == "Pedido Finalizado"}">
                        <div class="card statusFinalizadoPedido">
                        <div class="card-content ">
                       <div class="card-top">
                       <span class="new badge black white-text" data-badge-caption="${cardapioMedico?.status?.nome}">
                    </g:elseif>
                    <g:else>
                        <div class="card">
                       <div class="card-content">
                      <div class="card-top">
                      <span class="new badge" data-badge-caption="${cardapioMedico?.status?.nome}">
                    </g:else>
                    Status:
                </span>

                    <div class="font-small"><b>Data de entrega solicitada:</b> ${cardapioMedico?.pedido?.format('dd/MM/yyyy HH:mm')}
                    </div>
                    <span class="font-small"><b>Refeição:</b> ${cardapioMedico?.refeicao?.nome}</span>

                    </div>

                    <div class="card-action mt1rem">
                     <div class="">
                     <b>Data da Solicitação:</b> ${cardapioMedico?.solicitacao?.format('dd/MM/yyyy HH:mm')}
                    </div>

                    <div>
                        <b>Observação:</b>
                        ${cardapioMedico?.observacao}
                    </div>
                    <div class="font-bold mb15">
                        Montagem:
                    </div>
                    <g:each in="${cardapioMedico?.item}" var="item">
                        <g:if test="${item?.categoria?.nome == "Carboidrato"}">
                            <div class="chipCategoriaItem chipColorCarboidrato">
                        </g:if>
                        <g:if test="${item?.categoria?.nome == "Salada"}">
                            <div class="chipCategoriaItem chipColorSalada">
                        </g:if>
                        <g:if test="${item?.categoria?.nome == "Proteína Vegetal"}">
                            <div class="chipCategoriaItem chipColorProteinaVegetal">
                        </g:if>
                        <g:if test="${item?.categoria?.nome == "Proteína Animal"}">
                            <div class="chipCategoriaItem chipColorProteinaAnimal">
                        </g:if>
                        <g:if test="${item?.categoria?.nome == "Adicional"}">
                            <div class="chipCategoriaItem chipColorAdicional">
                        </g:if>
                        <g:if test="${item?.categoria?.nome == "Bebida"}">
                            <div class="chipCategoriaItem chipColorBebidas">
                        </g:if>
                        <g:if test="${item?.categoria?.nome == "Guarnição"}">
                            <div class="chipCategoriaItem chipColorGuarnicao">
                        </g:if>
                        <g:if test="${item?.categoria?.nome == "Lanche"}">
                            <div class="chipCategoriaItem chipColorBebidas">
                        </g:if>
                        <li>${item?.nome}</li>
                        </div>
                    </g:each>
                    </div>
                    <p class="left-align">
                    %{--action="cancelarPedido" controller="cardapio" id="${cardapioMedico.id}"--}%
                        <g:if test="${cardapioMedico?.status?.nome != "Cancelado" && cardapioMedico?.status?.nome != "Entregue" && cardapioMedico?.status?.nome != "Pedido Finalizado"}">
                            <button data-target="modalInfoCancelar"
                                    class="btn btn-small modal-trigger red white-text mtBtn10">Cancelar Pedido</button>
                        </g:if>
                        <g:link action="show" controller="cardapio" class="btn btn-small blue mtBtn10"
                                id="${cardapioMedico.id}">Detalhes</g:link>
                    </p>
                    <!-- Modal Structure -->
                    <div id="modalInfoCancelar" class="modal center-align">
                        <div class="modal-content ">
                            <h4 class="red-text">Espere um pouco!!</h4>

                            <p class="fontStronger">Voce tem certeza que deseja cancelar seu pedido?</p>

                            <p>Seu pedido após o cancelamento só irá desaparecer da lista, após validação da Cozinha.</p>
                        </div>

                        <div class="modal-footer center mb1rem">
                            <a href="#!" class="modal-close waves-effect waves-green btn grey mr1">Voltar</a>
                            <g:link action="cancelarPedidoMedico" controller="cardapio" id="${cardapioMedico.id}"
                                    class="waves-effect waves-green btn green fontStronger mr1">Sim! Desejo.</g:link>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
                </g:each>
            </div>
        </section>
    </g:if>
    <g:else>
        <section class="container">
            <div class="center-align mt150">
                <div class="card">
                    <div class="card-content">
                        <h6 class="">Voce ainda não possui nenhum pedido... Clique abaixo para pedir algo.</h6>
                        <g:link controller="cardapio" action="create"
                                class="btn waves-effect waves-light blue-grey hoverable mt25"><i
                                class="material-icons left white-text">local_dining</i>Solicitar refeição</g:link>
                    </div>
                </div>

                <div class="fixed-action-btn">
                    <g:link controller="cardapio" action="create" class="btn-floating btn-large cyan darken-1 pulse">
                        <i class="material-icons white-text">local_dining</i>
                    </g:link>
                </div>
            </div>
        </section>
    </g:else>
</main>
</body>
</html>