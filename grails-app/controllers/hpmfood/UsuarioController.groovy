package hpmfood

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UsuarioController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() {
       List <Usuario> usuarios = Usuario.executeQuery('''from Usuario order by pessoa.nome''')
        respond (usuarios:usuarios)
    }

    def show(Usuario usuario) {
        respond usuario
    }

    def create() {

        String isFuncionario = 'S'
        List<Perfil> listPerfil = Perfil.findAll("from Perfil order by nome")
        List<Pessoa> funcionarios = Pessoa.executeQuery('''from Pessoa where isFuncionario = :isFuncionario order by nome''', [isFuncionario: isFuncionario])

        respond (new Usuario(params), model: [perfis: listPerfil, funcionarios: funcionarios])
    }

    @Transactional
    def save(def params) {

        Usuario usuario = new Usuario()
//        Boolean ativo = Boolean.parseBoolean(params.exclusaoLogica)

        usuario.login = params.login
        usuario.exclusaoLogica = params.isAtivo
        usuario.senha = params.senha
        usuario.perfil = Perfil.findById(params.perfil)
        usuario.pessoa = Pessoa.findById(params.pessoa)

        usuario.save flush:true, failOnError: true
        redirect(action: 'index')
    }

    def edit(Usuario usuario) {

        String isFuncionario = 'S'
        List<Perfil> listPerfil = Perfil.findAll("from Perfil order by nome")
        List<Pessoa> funcionarios = Pessoa.executeQuery('''from Pessoa where isFuncionario = :isFuncionario order by nome''', [isFuncionario: isFuncionario])

        respond usuario, model: [perfis: listPerfil, funcionarios: funcionarios]
    }

    @Transactional
    def update(def params) {

        Usuario usuario = Usuario.get(params.id)
        usuario.login = params.login
        usuario.senha = params.senha
        usuario.perfil = Perfil.findById(params.perfil)
        usuario.pessoa = Pessoa.findById(params.pessoa)

        usuario.save flush:true, failOnError: true

        redirect(action: 'index')
    }

    @Transactional
    def delete(Usuario usuario) {

        if (usuario == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        usuario.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'usuario.label', default: 'Usuario'), usuario.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'usuario.label', default: 'Usuario'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
