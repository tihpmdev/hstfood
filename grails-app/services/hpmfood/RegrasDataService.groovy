package hpmfood

import grails.transaction.Transactional

@Transactional
class RegrasDataService {

    Calendar retornaUltimoDia() {
        Calendar mesAnoUltimoDia = Calendar.getInstance()
        int ultimoDia = mesAnoUltimoDia.getActualMaximum(Calendar.DAY_OF_MONTH)
        mesAnoUltimoDia.set(Calendar.DAY_OF_MONTH, ultimoDia)
        mesAnoUltimoDia.clearTime()
        mesAnoUltimoDia
    }

    Calendar retornaPrimeiroDia() {
        Calendar mesAnoPrimeiroDia = Calendar.getInstance()
        mesAnoPrimeiroDia.set(Calendar.DAY_OF_MONTH, 1)
        mesAnoPrimeiroDia.clearTime()
        mesAnoPrimeiroDia
    }

    Calendar retornaMesPorExtenso(params) {

        if (params.mes == null || params.mes == "null") {
            params.mes = new Date().format("MM")
            params.ano = new Date().format("yyyy")
        }

        String mesParametro = params.mes
        int numeroMes = Integer.valueOf(mesParametro)

        Calendar mesExtenso = Calendar.getInstance()
        mesExtenso.set(Calendar.MONTH, numeroMes - 1)
        mesExtenso
    }

    Calendar retornaUltimaHoraDia() {
        Calendar dataUltimaHora = Calendar.getInstance()
        dataUltimaHora.set(Calendar.HOUR, 23)
        dataUltimaHora.set(Calendar.MINUTE, 59)
        dataUltimaHora.set(Calendar.SECOND, 59)
        return dataUltimaHora
    }

    Date retornaHoraLimiteAlmoco() {
        Calendar dataLimiteAlmoco = Calendar.getInstance()
        dataLimiteAlmoco.set(Calendar.HOUR_OF_DAY, 10)
        dataLimiteAlmoco.set(Calendar.MINUTE, 00)
        dataLimiteAlmoco.set(Calendar.SECOND, 00)
        return dataLimiteAlmoco.getTime()
    }

    Date retornaHoraLimiteJantar() {
        Calendar dataLimiteJantar = Calendar.getInstance()
        dataLimiteJantar.set(Calendar.HOUR_OF_DAY, 15)
        dataLimiteJantar.set(Calendar.MINUTE, 00)
        dataLimiteJantar.set(Calendar.SECOND, 00)
        return dataLimiteJantar.getTime()
    }


}
