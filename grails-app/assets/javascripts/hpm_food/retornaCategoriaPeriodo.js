function periodoDados() {

    var urlListRefeicao = $('#listPeriodo').val();
    var periodo = $('#periodo_id').val();

    $('#refeicao_id option').each(function () {
        $(this).remove();
    });

    $.ajax({
        url: urlListRefeicao,
        type: "post",
        data: "periodoId=" + periodo,

        success: function (data) {

            var refeicoes = data;
            $('#refeicao_id').append('<option class="selectJumpCat" value="">'+"Selecione a refeição"+'</option>')
            for (var item = 0; item < refeicoes.length; item++) {

                var refId = refeicoes[item].id;
                $('#refeicao_id').append('<option class="selectJumpCat" value="' + refId + '">' + refeicoes[item].nome + '</option>')
            }
            $('#refeicao_id').removeAttr('disabled')
        },
        error: function (request, status, error) {
            console.log(request, status, error);
        }
    });
}
