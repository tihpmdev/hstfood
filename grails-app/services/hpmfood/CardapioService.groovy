package hpmfood

import grails.transaction.Transactional

@Transactional
class CardapioService {

    def listRefeicaoPorPeriodo(Long periodo){

        List <Refeicao> refeicoes = Refeicao.findAll('''from Refeicao as ref where ref.periodo.id= :periodo''', [periodo: periodo])
        return refeicoes

    }

    List retornaConsultaPedidoInterno(params) {

        List<Setor> setores = Setor.executeQuery('''from Setor order by nome asc''')
        List<Refeicao> refeicoes = Refeicao.executeQuery('''from Refeicao order by nome asc''')
        List<Status> statusList = Status.executeQuery('''from Status order by nome asc''')
        List<Cardapio> cardapios = []

        Medico medicoParams = null
        Setor setorParams = null
        Refeicao refeicaoParams = null
        Status statusParams = null
        String horaInicial = " 00:00"
        String horaFinal = " 23:59"
        Date dataParamsEntregaInicial = null
        Date dataParamsEntregaFinal = null

        if (params?.buscaMedico) {
            Pessoa solicitante = Pessoa.findByNome(params.buscaMedico)
            medicoParams = Medico.findByPessoa(solicitante)
        }
        if (params?.buscaSetor) {
            setorParams = Setor.findById(params.buscaSetor)
        }
        if (params?.buscaRefeicao) {
            refeicaoParams = Refeicao.findById(params.buscaRefeicao)
        }
        if (params?.buscaStatus) {
            statusParams = Status.findById(params.buscaStatus)
        }
        if (params.buscaDataEntrega) {
            dataParamsEntregaInicial = new Date().parse('dd/MM/yyyy HH:mm', params.buscaDataEntrega + horaInicial)
            dataParamsEntregaFinal = new Date().parse('dd/MM/yyyy HH:mm', params.buscaDataEntrega + horaFinal)

            println(dataParamsEntregaInicial)
            println(dataParamsEntregaFinal)
        }

        if (params?.buscaMedico != null || params?.buscaRefeicao != null || params?.buscaSetor != null || params?.buscaStatus != null) {
            cardapios = Cardapio.withCriteria {
                eq('exclusaoLogica', false)
                if (statusParams) {
                    like('status', statusParams)
                }
                if (medicoParams) {
                    and { like('medico', medicoParams) }
                }
                if (setorParams) {
                    and { like('setor', setorParams) }
                }
                if (refeicaoParams) {
                    and { like('refeicao', refeicaoParams) }
                }
                if (dataParamsEntregaInicial) {
                    and { between('pedido', dataParamsEntregaInicial, dataParamsEntregaFinal) }
                }
                order('pedido', 'desc')

            } as List<Cardapio>
        }
        [setores, refeicoes, statusList, cardapios]
    }

}
