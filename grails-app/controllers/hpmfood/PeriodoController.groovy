package hpmfood

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PeriodoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() {
        List<Periodo> periodos = Periodo.findAll(''' from Periodo order by nome''')
        respond(periodo: periodos)
    }

    def show(Periodo periodo) {
        respond periodo
    }

    def create() {
        respond new Periodo(params)
    }

    @Transactional
    def save(def params) {


        println(params.inicio)

        Periodo periodo = new Periodo()
        periodo.nome = params.nome
        periodo.horaInicial = params?.inicio
        periodo.horaFinal = params?.fim

        periodo.save(flush: true, failOnError: true)
        redirect(view: 'index')

    }
    def edit(Periodo periodo) {
        respond periodo
    }

    @Transactional
    def update(Periodo periodo) {
        if (periodo == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (periodo.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond periodo.errors, view:'edit'
            return
        }

        periodo.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'periodo.label', default: 'Periodo'), periodo.id])
                redirect periodo
            }
            '*'{ respond periodo, [status: OK] }
        }
    }

    @Transactional
    def delete(Periodo periodo) {

        if (periodo == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        periodo.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'periodo.label', default: 'Periodo'), periodo.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'periodo.label', default: 'Periodo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
