<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'agendaCirurgica.label', default: 'AgendaCirurgica')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<main class="container">
    <g:if test="${agendas != []}">
        <section>
            <h5 class="center-align mt2rem mb2rem">Agenda Cirurgica - ${new Date().format('MMMM')}

                <p class="mt1rem font-normal">${session?.usuario?.nome}</p>

            </h5>
        </section>
        <section class="row">
            <ul class="collapsible" data-collapsible="accordion">
                <g:each in="${agendas}" var="agenda">
                    <li>
                        <div class="collapsible-header font-bold">

                            <div class="col s4 m3 l3">
                                <i class="material-icons">date_range</i>
                                <span>${agenda?.data_agenda?.format('dd/MM/yyyy')}
                                </span>
                            </div>

                            <div class="col s8 m9 l9">
                                <div class="truncate left-align">${agenda?.procedimento?.nome}</div>
                            </div>
                        </div>

                        <div class="collapsible-body fundoCollapse">
                            <ul class="collection">
                                <li class="collection-item">
                                    <div class="title font-bold col s10">
                                        <span>Procedimento: ${agenda?.procedimento?.nome}</span>
                                    </div>

                                    <div class="secondary-content font-bold black-text col s2 right-align">
                                        <span>${agenda?.nome_sala}</span>
                                    </div>

                                    <div class="col s12">
                                        <p>Paciente: ${agenda?.paciente?.nome}</p>

                                        <p>Convenio: ${agenda?.convenio?.nome}</p>

                                        <p>Cód. atendimento: ${agenda?.cod_atendimento}</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
                </g:each>
            </ul>
        </section>
    </g:if>
    <g:else>
        <section class="mt150">
            <div class="card center-align">
                <div class="card-content">
                    <h5>Voce não possui nenhuma cirurgia agendada esse mês... Por enquanto!</h5>
                    <i class="material-icons green-text iconeGrande mt25">beach_access</i>
                </div>
            </div>
        </section>
    </g:else>
</main>
<script type="text/javascript">

</script>
</body>
</html>