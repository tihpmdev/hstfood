package hpmfood

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class RegistroConversaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond RegistroConversa.list(params), model: [registroConversaCount: RegistroConversa.count()]
    }

    def show(RegistroConversa registroConversa) {
        respond registroConversa
    }

    def create() {
        respond new RegistroConversa(params)
    }

    @Transactional
    def save(def params) {

        if (session?.usuario) {

            RegistroConversa registroConversa = new RegistroConversa()

            Long parsePessoaLogada = Long.parseLong(params.pessoa)

            Cardapio cardapio = Cardapio.get(params.cardapio)

            if (parsePessoaLogada == cardapio?.medico?.pessoa?.id ){

                registroConversa.pessoa = Pessoa.findById(parsePessoaLogada)
                registroConversa.cardapio = Cardapio.findById(params?.cardapio)
                registroConversa.envio = new Date()
                registroConversa.mensagem = params.mensagem

            }else{
                registroConversa.pessoa = Pessoa.findById(parsePessoaLogada)
                registroConversa.cardapio = Cardapio.findById(params?.cardapio)
                registroConversa.envio = new Date()
                registroConversa.mensagem = params.mensagem
            }

            registroConversa.save flush: true, failOnError: true
            redirect(controller: 'cardapio', action: 'show', id:registroConversa.cardapio.id)
        }

    }

    def edit(RegistroConversa registroConversa) {
        respond registroConversa
    }

    @Transactional
    def update(RegistroConversa registroConversa) {
        if (registroConversa == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (registroConversa.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond registroConversa.errors, view: 'edit'
            return
        }

        registroConversa.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'registroConversa.label', default: 'RegistroConversa'), registroConversa.id])
                redirect registroConversa
            }
            '*' { respond registroConversa, [status: OK] }
        }
    }

    @Transactional
    def delete(RegistroConversa registroConversa) {

        if (registroConversa == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        registroConversa.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'registroConversa.label', default: 'RegistroConversa'), registroConversa.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'registroConversa.label', default: 'RegistroConversa'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
