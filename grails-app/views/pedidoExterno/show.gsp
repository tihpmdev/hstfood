<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'pedidoExterno.label', default: 'PedidoExterno')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<main class="mt25">
    <header>
        <h4 class="center">Detalhe do Pedido</h4>
    </header>
    <section class="row containerJumpcatIndex">
        <div class="col s12 m12 l12">
            <div class="row">
                <div class="col s12 m12 l12">

                        <g:if test="${pedidoExterno?.status?.nome == "Novo Pedido"}">
                    <div class="card statusNovoPedido">
                        <div class="card-content">
                            <div class="card-top">
                            <span class="new badge blue" data-badge-caption="${pedidoExterno?.status?.nome}">
                        </g:if>
                        <g:elseif test="${pedidoExterno?.status?.nome == "Em preparo"}">
                            <div class="card statusEmPreparo">
                            <div class="card-content">
                           <div class="card-top">
                           <span class="new badge purple" data-badge-caption="${pedidoExterno?.status?.nome}">
                        </g:elseif>
                        <g:elseif test="${pedidoExterno?.status?.nome == "Saiu para entrega"}">
                            <div class="card statusEmMovimento">
                            <div class="card-content ">
                           <div class="card-top">
                           <span class="new badge orange" data-badge-caption="${pedidoExterno?.status?.nome}">
                        </g:elseif>
                        <g:elseif test="${pedidoExterno?.status?.nome == "Entregue"}">
                            <div class="card statusEntregue">
                            <div class="card-content ">
                           <div class="card-top">
                           <span class="new badge green" data-badge-caption="${pedidoExterno?.status?.nome}">
                        </g:elseif>
                        <g:elseif test="${pedidoExterno?.status?.nome == "Cancelado"}">
                            <div class="card statusCancelado">
                            <div class="card-content ">
                           <div class="card-top">
                           <span class="new badge red" data-badge-caption="${pedidoExterno?.status?.nome}">
                        </g:elseif>
                        <g:else>
                            <div class="card">
                           <div class="card-content">
                          <div class="card-top">
                          <span class="new badge" data-badge-caption="${pedidoExterno?.status?.nome}">
                        </g:else>
                        Status:
                    </span>
                        <span class="font-small"><b>Refeição:</b> ${pedidoExterno?.refeicao?.nome}</span>

                        </div>

                        <hr>

                         <div class=""><b>Data da Solicitação:</b> ${pedidoExterno?.solicitacao?.format('dd/MM/yyyy HH:mm')}</div>
                         <div class=""><b>Dieta:</b> ${pedidoExterno?.dieta}</div>
                         <div class=""><b>Setor:</b> ${pedidoExterno?.setor}</div>
                         <div class=""><b>Solicitante:</b> ${pedidoExterno?.solicitante}</div>
                         <div class=""><b>Paciente:</b> ${pedidoExterno?.paciente}</div>
                         <div class=""><b>Observação:</b> ${pedidoExterno?.observacao}</div>
                         <g:if test="${pedidoExterno?.temAcompanhante == true}">
                                 <div class=""><b>Possui acompanhante?:</b> Possui</div>
                            </g:if>
                            <g:elseif test="${pedidoExterno?.temAcompanhante == false}">
                                <div class=""><b>Acompanhante:</b> Não possui</div>
                            </g:elseif>

                        </div>
                    </div>
                </div>
             </div>
         </div>

    </section>
</main>
</body>
</html>
