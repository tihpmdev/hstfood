<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'pedidoExterno.label', default: 'PedidoExterno')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<main class="mt25">
    <g:if test="${flash.message}">
        <section class="container center mt1rem">
            <h4 class="red-text font-bold">${flash.message}</h4>
        </section>
    </g:if>
    <section class="container row">

        <h5 class="center-align mt2rem">Painel de Pedidos Paciente</h5>

        <div class="container col s12 m12 l12 infoBackground">
            <span class="col s12 m12 l12 font-small center-align">Ordenados em  recentes solicitados</span>
        </div>

    </section>

    <g:if test="${pedidos == []}">
        <section class="container">
            <div class="center-align">
                <div class="card">
                    <div class="card-content">
                        <h5 class="">Voce ainda não possui nenhum pedido... Clique abaixo para pedir algo.</h5>
                        <g:link controller="pedidoExterno" action="create"
                                class="btn waves-effect waves-light blue-grey hoverable mt25"><i
                                class="material-icons left white-text">local_dining</i>Solicitar refeição</g:link>
                    </div>
                </div>
            </div>
        </section>
    </g:if>
    <section class="containerJumpcatIndex">

        <ul class="collapsible popout">
            <g:each in="${pedidos}" var="pedExt">
                <li>
                <li id="">
                    <g:if test="${pedExt?.status?.nome == "Novo Pedido"}">
                        <div class="collapsible-header row no-margin-bottom statusNovoPedido">
                    </g:if>
                    <g:elseif test="${pedExt?.status?.nome == "Em preparo"}">
                        <div class="collapsible-header row no-margin-bottom statusEmPreparo">
                    </g:elseif>
                    <g:elseif test="${pedExt?.status?.nome == "Saiu para entrega"}">
                        <div class="collapsible-header row no-margin-bottom statusEmMovimento">
                    </g:elseif>
                    <g:elseif test="${pedExt?.status?.nome == "Entregue"}">
                        <div class="collapsible-header row no-margin-bottom statusEntregue">
                    </g:elseif>
                    <g:elseif test="${pedExt?.status?.nome == "Cancelado"}">
                        <div class="collapsible-header row no-margin-bottom statusCancelado">
                    </g:elseif>
                    <g:elseif test="${pedExt?.status?.nome == "Pedido Finalizado"}">
                        <div class="collapsible-header row no-margin-bottom statusFinalizadoPedido">
                    </g:elseif>
                    <g:else>
                        <div class="collapsible-header row no-margin-bottom">
                    </g:else>

                    <div class="col s9 m4 xl2 font-small">
                        <span class="font-bold">Local:</span>
                        ${pedExt?.setor?.nome}
                        <br>



                        <g:if test="${pedExt?.status?.nome == "Novo Pedido"}">
                            <span class="blue-text mt1rem"><strong>Data de Entrega:</strong>
                                <g:if test="${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm') == null}">
                                    Sem data de entrega
                                </g:if>
                                <g:else>
                                    ${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm')}
                                </g:else>
                            </span>
                        </g:if>
                        <g:elseif test="${pedExt?.status?.nome == "Em preparo"}">
                            <span class="purple-text mt1rem blue"><strong>Data de Entrega:</strong>
                                <g:if test="${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm') == null}">
                                    Sem data de entrega
                                </g:if>
                                <g:else>
                                    ${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm')}
                                </g:else>
                            </span>
                        </g:elseif>
                        <g:elseif test="${pedExt?.status?.nome == "Saiu para entrega"}">
                            <span class="orange-text mt1rem"><strong>Data de Entrega:</strong>
                                <g:if test="${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm') == null}">
                                    Sem data de entrega
                                </g:if>
                                <g:else>
                                    ${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm')}
                                </g:else>
                            </span>
                        </g:elseif>
                        <g:elseif test="${pedExt?.status?.nome == "Entregue"}">
                            <span class=" green-text mt1rem"><strong>Data de Entrega:</strong>
                                <g:if test="${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm') == null}">
                                    Sem data de entrega
                                </g:if>
                                <g:else>
                                    ${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm')}
                                </g:else>
                            </span>
                        </g:elseif>
                        <g:elseif test="${pedExt?.status?.nome == "Cancelado"}">
                            <span class="red-text mt1rem"><strong>Data de Entrega:</strong>
                                <g:if test="${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm') == null}">
                                    Sem data de entrega
                                </g:if>
                                <g:else>
                                    ${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm')}
                                </g:else>
                            </span>
                        </g:elseif>
                        <g:elseif test="${pedExt?.status?.nome == "Pedido Finalizado"}">
                            <span class="black-text mt1rem"><strong>Data de Entrega:</strong>
                                <g:if test="${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm') == null}">
                                    Sem data de entrega
                                </g:if>
                                <g:else>
                                    ${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm')}
                                </g:else>
                            </span>
                        </g:elseif>
                        <g:else>
                            <span class="mt1rem"><strong>Data de Entrega:</strong>
                                <g:if test="${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm') == null}">
                                    Sem data de entrega
                                </g:if>
                                <g:else>
                                    ${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm')}
                                </g:else>
                            </span>
                        </g:else>

                    </div>

                    <div class="col s12 m4 xl2 font-small">
                        <span class="font-bold">Paciente:</span>
                        ${pedExt?.paciente}
                    </div>


                    <div class="col s12 m4 xl2 font-small">
                        <span class="font-bold">Dieta:</span>
                        ${pedExt?.dieta}
                        <br>
                        <span class="font-bold">Patologia(s):</span>
                        <g:each in="${pedExt?.patologia}" var="pat">
                            <div>
                                ${pat?.nome}
                            </div>
                        </g:each>

                    </div>

                    <div class="col s12 m4 xl2 font-small">
                        <span class="font-bold">Observação:</span>
                        ${pedExt?.observacao}
                    </div>

                    <div class="col s12 m4 xl2 jumpCardDashboard right-align">
                        <span class="font-bold mr1 ">Acompanhante:</span>
                        <g:if test="${pedExt?.temAcompanhante == true}">
                            <span class="new badge green show-on-small" data-badge-caption="SIM">
                        </g:if>
                        <g:elseif test="${pedExt?.temAcompanhante == false}">
                            <span class="new badge red show-on-small" data-badge-caption="NÃO">
                        </g:elseif>
                    </span>
                    </div>
                </div>

                    <div class="collapsible-body fundoCollapse">
                        <span class="mt-25">
                            <div class="right-align">
                                <g:if test="${cardapioMedico?.status?.nome == "Novo Pedido"}">
                                    <span class="new badge" data-badge-caption="${pedExt?.status?.nome}"></span>
                                </g:if>
                                <g:elseif test="${cardapioMedico?.status?.nome == "Em preparo"}">
                                    <span class="new badge" data-badge-caption="${pedExt?.status?.nome}"></span>
                                </g:elseif>
                                <g:elseif test="${cardapioMedico?.status?.nome == "Saiu para entrega"}">
                                    <span class="new badge" data-badge-caption="${pedExt?.status?.nome}"></span>
                                </g:elseif>
                                <g:elseif test="${cardapioMedico?.status?.nome == "Entregue"}">
                                    <span class="new badge" data-badge-caption="${pedExt?.status?.nome}"></span>
                                </g:elseif>

                                <g:elseif test="${cardapioMedico?.status?.nome == "Cancelado"}">
                                    <span class="new badge" data-badge-caption="${pedExt?.status?.nome}"></span>
                                </g:elseif>

                                <g:elseif test="${cardapioMedico?.status?.nome == "Pedido Finalizado"}">
                                    <span class="new badge" data-badge-caption="${pedExt?.status?.nome}"></span>
                                </g:elseif>
                                <g:else>
                                    <span class="new badge" data-badge-caption="${pedExt?.status?.nome}"></span>
                                </g:else>
                        </span>
                    </div>

                    <div class="font-small">

                        <div class="col s2 m12 l12">
                            <span class="font-bold">Pedido:</span>
                            ${pedExt?.id}
                        </div>

                        <div class="col s2 m12 l12">
                            <span class="font-bold">Refeição:</span>
                            ${pedExt?.refeicao?.nome}
                        </div>

                        <div class="col s2 m12 l12">
                            <span class="font-bold">Solicitado em:</span>
                            ${pedExt?.solicitacao?.format('dd/MM/yyyy HH:mm')}
                        </div>

                        <div class="col s2 m12 l12">
                            <span class="font-bold">Local:</span>
                            ${pedExt?.setor?.nome}
                        </div>

                        <div class="col s2 m12 l12">
                            <span class="font-bold">Dieta:</span>
                            ${pedExt?.dieta}
                        </div>

                        %{--<div class="col s12 m4 xl2 font-small">--}%
                        %{--<span class="hide-on-med-and-down font-bold">Patologia(s):</span>--}%
                        %{--<g:each in="${pedExt?.patologia}" var="pat">--}%
                        %{--<div class="chip">--}%
                        %{--${pat?.nome}--}%
                        %{--</div>--}%
                        %{--</g:each>--}%
                        %{--</div>--}%
                    </div>

                </span>

                    <p class="">
                        <g:if test="${pedExt?.status?.nome == "Novo Pedido"}">
                            <g:link action="iniciarPreparo" controller="pedidoExterno" id="${pedExt.id}"
                                    class="btn btn-small purple white-text mtBtn10">Iniciar Preparo</g:link>
                        </g:if>
                        <g:if test="${pedExt?.status?.nome == "Em preparo"}">
                            <g:link action="encaminharPedidoEstabelecimento" controller="pedidoExterno"
                                    id="${pedExt.id}"
                                    class="btn btn-small orange white-text mtBtn10">Encaminhar para estabelecimento</g:link>
                        </g:if>
                        <g:if test="${pedExt?.status?.nome == "Saiu para entrega"}">
                            <g:link action="entregarPedidoEstabelecimento" controller="pedidoExterno"
                                    id="${pedExt.id}"
                                    class="btn btn-small green white-text mtBtn10">Entregue no estabelecimento</g:link>
                        </g:if>
                        <g:if test="${pedExt?.status?.nome == "Cancelado"}">
                            <g:link action="reabrirPedido" controller="pedidoExterno" id="${pedExt.id}"
                                    class="btn btn-small blue-grey white-text mtBtn10">Reabrir pedido</g:link>
                        </g:if>
                        <g:if test="${pedExt?.status?.nome == "Entregue"}">
                            <g:link action="finalizarPedido" controller="pedidoExterno" id="${pedExt.id}"
                                    class="btn btn-small black white-text mtBtn10">Finalizar Pedido</g:link>
                        </g:if>
                        <g:if test="${pedExt?.status?.nome == "Pedido Finalizado"}">
                            <g:link action="reabrirPedido" controller="pedidoExterno" id="${pedExt.id}"
                                    class="btn btn-small blue-grey white-text mtBtn10">Reabrir pedido</g:link>
                        </g:if>
                        <g:if test="${pedExt?.status?.nome != "Cancelado" && pedExt?.status?.nome != "Entregue" && pedExt?.status?.nome != "Pedido Finalizado"}">
                            <g:link action="cancelarPedido" controller="pedidoExterno" id="${pedExt.id}"
                                    class="btn btn-small red white-text mtBtn10">Cancelar Pedido</g:link>
                        </g:if>
                        <g:if test="${session?.usuario?.perfil == "Administrador"}">
                            <g:link action="excluirPedido" controller="pedidoExterno" id="${pedExt.id}"
                                    class="btn btn-small font-bold white red-text mtBtn10">Excluir</g:link>
                        </g:if>

                        %{--<g:link action="show" controller="pedidoExterno" class="btn btn-small blue mtBtn10"--}%
                        %{--id="${pedExt.id}">Detalhes</g:link>--}%
                    </p>
                </div>
                </li>
            </g:each>
        </ul>
    </section>
    <section class="mt-25">
        <div class="fixed-action-btn">
            <g:link action="relPedidosExterno" class="btn-floating btn-large white hoverable">
                <i class="large material-icons red-text">search</i>
            </g:link>
        </div>
    </section>

</main>
</body>
</html>