<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'patologia.label', default: 'Patologia')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<main class="mb15 mt25 container">

    <section class="row">
        <g:link controller="patologia" action="create" class="btn green">Cadastrar Patologia</g:link>
    </section>

    <section>
        <g:if test="${flash.message}">
            <p class="center">${flash.message}</p>
        </g:if>
        <h5 class="center-align mb15">Lista de Patologias</h5>
        <table class="table white striped">
            <thead>
            <tr>
                <th>Nome</th>
                <th>Observação</th>
                <th  class="right-align">Ações</th>
            </tr>
            </thead>
            <tbody class="">
            <g:each in="${patologias}" var="pat">
                <tr>
                    <td>${pat?.nome}</td>
                    <td>${pat?.observacao}</td>

                    <td class="right-align">
                        <g:link class="btn yellow btn-small" action="edit" id="${pat.id}"><i class="large material-icons">create</i></g:link>
                        <g:link class="btn red btn-small" action="excluirPatologia" id="${pat.id}"><i class="large material-icons">delete</i></g:link>
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </section>
    <section>
        <div class="fixed-action-btn">
            <g:link action="create" class="btn-floating btn-large green">
                <i class="large material-icons">add</i>
            </g:link>
        </div>
    </section>
</main>
</body>
</html>