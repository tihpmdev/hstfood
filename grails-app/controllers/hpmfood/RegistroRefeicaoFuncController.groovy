package hpmfood

import grails.converters.JSON

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class RegistroRefeicaoFuncController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    RegistroRefeicaoFuncService registroRefeicaoFuncService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond RegistroRefeicaoFunc.list(params), model:[registroRefeicaoFuncCount: RegistroRefeicaoFunc.count()]
    }

    def show(RegistroRefeicaoFunc registroRefeicaoFunc) {
        respond registroRefeicaoFunc
    }

    def create() {
        respond new RegistroRefeicaoFunc(params)
    }

    def retornaPessoaDados(){


        def dadosFuncionario = Pessoa.findByCpf(params.cpfDados)
        render(dadosFuncionario as JSON)
    }

    @Transactional
    def save(def params) {

        Pessoa pessoa = Pessoa.findByCpf(params.cpfDados)

        RegistroRefeicaoFunc registro = new RegistroRefeicaoFunc()
        registro.funcionario = pessoa
        registro.entradaAlmoco = new Date()
        registro.save(flush: true, failOnError: true)

        flash.message = "Olá ${pessoa.nome} seu Registro de refeição foi efetuado"
        redirect(action: 'create')

    }

    def edit(RegistroRefeicaoFunc registroRefeicaoFunc) {
        respond registroRefeicaoFunc
    }

    @Transactional
    def update(RegistroRefeicaoFunc registroRefeicaoFunc) {
        if (registroRefeicaoFunc == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (registroRefeicaoFunc.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond registroRefeicaoFunc.errors, view:'edit'
            return
        }

        registroRefeicaoFunc.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'registroRefeicaoFunc.label', default: 'RegistroRefeicaoFunc'), registroRefeicaoFunc.id])
                redirect registroRefeicaoFunc
            }
            '*'{ respond registroRefeicaoFunc, [status: OK] }
        }
    }

    @Transactional
    def delete(RegistroRefeicaoFunc registroRefeicaoFunc) {

        if (registroRefeicaoFunc == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        registroRefeicaoFunc.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'registroRefeicaoFunc.label', default: 'RegistroRefeicaoFunc'), registroRefeicaoFunc.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'registroRefeicaoFunc.label', default: 'RegistroRefeicaoFunc'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
