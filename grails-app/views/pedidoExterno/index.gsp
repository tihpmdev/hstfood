<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'pedidoExterno.label', default: 'PedidoExterno')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<main>
    <header class="center container">
       <h4>Lista de Pedidos Paciente</h4>
    </header>
    <section class="mt1rem mb1rem container">
        <table class="striped">
            <thead>
            <th>Solicitado em</th>
            <th>Local</th>
            <th>Paciente</th>
            <th>Dieta</th>
            <th>Refeicao</th>
            <th>Observação</th>
            <th>Status</th>
            <th class="center">Acompanhante</th>
            </thead>
            <tbody>
            <g:each in="${pedidoExterno}" var="pedExt">
                <tr>
                    <td>${pedExt?.solicitacao?.format('dd/MM/yyyy HH:mm')}</td>
                    <td>${pedExt?.setor?.nome}</td>
                    <td>${pedExt?.paciente}</td>
                    <td>${pedExt?.dieta?.nome}</td>
                    <td>${pedExt?.refeicao?.nome}</td>
                    <td>${pedExt?.observacao}</td>
                    <td>${pedExt?.status}</td>
                    <td class="center">
                        <g:if test="${pedExt?.temAcompanhante == true}">
                            Sim
                        </g:if>
                        <g:else>
                            Não
                        </g:else>
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </section>
</main>
</body>
</html>