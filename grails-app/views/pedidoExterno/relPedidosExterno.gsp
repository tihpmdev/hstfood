<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'pedidoExterno.label', default: 'PedidoExterno')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<main class="mt1rem mb1rem">
    <g:if test="${flash.message}">
        <section class="container center mt1rem">
            <h4 class="red-text font-bold">${flash.message}</h4>
        </section>
    </g:if>
    <section class="container row">

        <h5 class="center-align">Relatório - Pedidos Pacientes</h5>

        <div class="container col s12 m12 l12 mt25 center-align no-print">
            <button class="waves-effect red darken-4 waves-light btn" onclick="window.print()"><i class="material-icons left">local_printshop</i>Imprimir</button>
        </div>
    </section>


    <section class="containerJumpcat row no-print">
        <form>
            <div class="card white">
                <div class="card-content black-text">
                    <div>
                        <span class="center card-title">Selecione para buscar.</span>

                        <p>

                        <div class="input-field col s12 m3 l3">
                            <g:select name="buscaSetor" from="${setores}" optionKey="id"
                                      optionValue="${{ it.nome }}"
                                      id="setor_id" noSelection="['': 'Selecione o Local']"/>
                            <label for="setor_id">Local</label>
                        </div>

                        <div class="input-field col s12 m3 l3">
                            <g:select name="buscaRefeicao" from="${refeicoes}" optionKey="id"
                                      optionValue="${{ it.nome }}"
                                      id="refeicao_id" class="validate" noSelection="['': 'Selecione a Refeição']"/>
                            <label for="refeicao_id">Refeição</label>
                        </div>

                        <div class="input-field col s12 m3 l3">
                            <g:select name="buscaStatus" from="${status}" optionKey="id"
                                      optionValue="${{ it.nome }}"
                                      id="status_id" noSelection="['': 'Selecione o Status']"/>
                            <label for="status_id">Status</label>
                        </div>

                        <div class="input-field col s12 m3 l3">
                            <input type="text" onkeyup="mascaraData(this.value, this)"  name="buscaDataEntrega" id="id_entrega"/>
                            <label for="id_entrega">Data da Entrega</label>
                        </div>

                    </p>
                    </div>
                </div>

                <div class="center pb1rem">
                    <button type="submit" class="btn cyan btn-bloco">Buscar</button>
                </div>
            </div>
        </form>
    </section>
    <section class="containerTableRel">
        <g:if test="${pedidos == []}">
            <div class="card white">
                <div class="card-content  black-text">
                    <h5 class="center mb2rem">Nenhum registro encontrado...</h5>
                </div>
            </div>
        </g:if>
        <g:else>
            <table class="browser-defatul borderedTable">
                <thead class="borderedTable">
                <tr class="borderedTable">
                    <th class="borderedTable" width="12%">Entrega</th>
                    <th class="borderedTable" width="2%">Código</th>
                    <th class="borderedTable" width="8%">Local</th>
                    <th class="borderedTable">Paciente</th>
                    <th class="borderedTable" width="5%">Dieta</th>
                    <th class="borderedTable">Patologia</th>
                    <th class="borderedTable">Refeição</th>
                    <th class="borderedTable">Observação</th>
                    <th class="borderedTable" width="5%">Acompanhante</th>
                </tr>
                </thead>

                <tbody>
                <g:each in="${pedidos}" var="pedExt">
                    <tr>
                        <td class="borderedTable">${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm')}</td>
                        <td class="borderedTable sizeTd">${pedExt?.id}</td>
                        <td class="borderedTable">${pedExt?.setor?.nome}</td>
                        <td class="borderedTable">${pedExt?.paciente}</td>
                        <td class="borderedTable">${pedExt?.dieta}</td>
                        <td class="borderedTable">
                        <ul>
                            <g:each in="${pedExt?.patologia}" var="pat">

                                  <li class="font-bold"> ${pat?.nome}</li>

                            </g:each>
                        </ul>
                        </td>
                        <td class="borderedTable">${pedExt?.refeicao?.nome}</td>
                        <td class="borderedTable">${pedExt?.observacao}</td>
                        <td class="borderedTable">
                            <g:if test="${pedExt?.temAcompanhante == true}">
                                <span>SIM</span>
                            </g:if>
                            <g:elseif test="${pedExt?.temAcompanhante == false}">
                                <span>NÃO</span>
                            </g:elseif>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>

        </g:else>
    </section>
    <section class="mt-25 no-print">
        <div class="fixed-action-btn">
            <g:link action="dashBoardPedidoExterno" class="btn-floating btn-large white hoverable">
                <i class="large material-icons red-text">arrow_back</i>
            </g:link>
        </div>
    </section>
</main>
<script type="text/javascript">
    function mascaraData(Data, campo) {
        var data = '';
        data = data + Data;
        if (data.length == 2) {
            data = data + '/';
            campo.value = data;
        }
        if (data.length == 5) {
            data = data + '/';
            campo.value = data;
        }
    }
</script>
</body>
</html>