package hpmfood

class Status {

    String nome
    String ordem
    String observacao
    Boolean exclusaoLogica

    static constraints = {
        ordem nullable: true
        observacao nullable: true
        exclusaoLogica nullable: true
    }

    String toString(){
        this.nome
    }
}
