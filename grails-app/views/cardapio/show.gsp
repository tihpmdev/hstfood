<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'cardapio.label', default: 'Cardapio')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<main class="mt25">
    <section class="row containerJumpcatIndex">
        <div class="col">
            <button class="btn white blue-grey-text" onclick='history.go(-1)'>Voltar</button>
        </div>
    </section>
    <section class="row containerJumpcatIndex">
        <div class="col s12 m12 l6">
            <div class="row">
                <div class="col s12 m12 l12">

                        <g:if test="${cardapio?.status?.nome == "Novo Pedido"}">
                    <div class="card statusNovoPedido">
                        <div class="card-content">
                            <div class="card-top">
                            <span class="new badge blue" data-badge-caption="${cardapio?.status?.nome}">
                        </g:if>
                        <g:elseif test="${cardapio?.status?.nome == "Em preparo"}">
                            <div class="card statusEmPreparo">
                            <div class="card-content">
                           <div class="card-top">
                           <span class="new badge purple" data-badge-caption="${cardapio?.status?.nome}">
                        </g:elseif>
                        <g:elseif test="${cardapio?.status?.nome == "Saiu para entrega"}">
                            <div class="card statusEmMovimento">
                            <div class="card-content ">
                           <div class="card-top">
                           <span class="new badge orange" data-badge-caption="${cardapio?.status?.nome}">
                        </g:elseif>
                        <g:elseif test="${cardapio?.status?.nome == "Entregue"}">
                            <div class="card statusEntregue">
                            <div class="card-content ">
                           <div class="card-top">
                           <span class="new badge green" data-badge-caption="${cardapio?.status?.nome}">
                        </g:elseif>
                        <g:elseif test="${cardapio?.status?.nome == "Cancelado"}">
                            <div class="card statusCancelado">
                            <div class="card-content ">
                           <div class="card-top">
                           <span class="new badge red" data-badge-caption="${cardapio?.status?.nome}">
                        </g:elseif>
                        <g:else>
                            <div class="card">
                           <div class="card-content">
                          <div class="card-top">
                          <span class="new badge black" data-badge-caption="${cardapio?.status?.nome}">
                        </g:else>
                        Status:
                    </span>

                        <div class="font-small"><b>Data de entrega solicitada:</b> ${cardapio?.pedido?.format('dd/MM/yyyy HH:mm')}
                        </div>
                        <p class="font-small"><b>Medico:</b> ${cardapio?.medico?.pessoa?.nome}</p>
                        <span class="font-small"><b>Refeição:</b> ${cardapio?.refeicao?.nome}</span>

                        </div>

                        <hr>

                         <div class=""><b>Data da Solicitação:</b> ${cardapio?.solicitacao?.format('dd/MM/yyyy HH:mm')}</div>

                        <div class="font-bold mb15">
                            Montagem:
                        </div>
                        <g:each in="${cardapio?.item}" var="item">
                            <g:if test="${item?.categoria?.nome == "Carboidrato"}">
                                <div class="chipCategoriaItem chipColorCarboidrato">
                            </g:if>
                            <g:if test="${item?.categoria?.nome == "Salada"}">
                                <div class="chipCategoriaItem chipColorSalada">
                            </g:if>
                            <g:if test="${item?.categoria?.nome == "Proteína Vegetal"}">
                                <div class="chipCategoriaItem chipColorProteinaVegetal">
                            </g:if>
                            <g:if test="${item?.categoria?.nome == "Proteína Animal"}">
                                <div class="chipCategoriaItem chipColorProteinaAnimal">
                            </g:if>
                            <g:if test="${item?.categoria?.nome == "Adicional"}">
                                <div class="chipCategoriaItem chipColorAdicional">
                            </g:if>
                            <g:if test="${item?.categoria?.nome == "Bebida"}">
                                <div class="chipCategoriaItem chipColorBebidas">
                            </g:if>
                            <g:if test="${item?.categoria?.nome == "Guarnição"}">
                                <div class="chipCategoriaItem chipColorGuarnicao">
                            </g:if>
                            <g:if test="${item?.categoria?.nome == "Lanche"}">
                                <div class="chipCategoriaItem chipColorBebidas">
                            </g:if>
                            <li>${item?.nome}</li>
                            </div>
                        </g:each>
                        <div class="mt1rem">

                         <g:if test="${session?.usuario?.perfil == "Administrador"}" >


                                            <g:if test="${cardapio?.status?.nome == "Novo Pedido"}">
                                                <g:link action="iniciarPreparo" controller="cardapio" id="${cardapio.id}"
                                                        class="btn btn-small purple white-text mtBtn10">Iniciar Preparo</g:link>
                                            </g:if>
                                            <g:if test="${cardapio?.status?.nome == "Em preparo"}">
                                                <g:link action="encaminharPedidoEstabelecimento" controller="cardapio"
                                                        id="${cardapio.id}"
                                                        class="btn btn-small orange white-text mtBtn10">Encaminhar para estabelecimento</g:link>
                                            </g:if>
                                            <g:if test="${cardapio?.status?.nome == "Saiu para entrega"}">
                                                <g:link action="entregarPedidoEstabelecimento" controller="cardapio"
                                                        id="${cardapio.id}"
                                                        class="btn btn-small green white-text mtBtn10">Entregue no estabelecimento</g:link>
                                            </g:if>
                                            <g:if test="${cardapio?.status?.nome == "Cancelado"}">
                                                <g:link action="reabrirPedido" controller="cardapio" id="${cardapio.id}"
                                                        class="btn btn-small blue-grey white-text mtBtn10">Reabrir pedido</g:link>
                                            </g:if>
                                            <g:if test="${cardapio?.status?.nome == "Entregue"}">
                                                <g:link action="finalizarPedido" controller="cardapio" id="${cardapio.id}"
                                                        class="btn btn-small black white-text mtBtn10">Finalizar Pedido</g:link>
                                            </g:if>
                                            <g:if test="${cardapio?.status?.nome == "Pedido Finalizado"}">
                                                <g:link action="reabrirPedido" controller="cardapio" id="${cardapio.id}"
                                                        class="btn btn-small blue-grey white-text mtBtn10">Reabrir pedido</g:link>
                                            </g:if>

                                            <g:if test="${cardapio?.status?.nome != "Cancelado" && cardapio?.status?.nome != "Entregue" && cardapio?.status?.nome != "Pedido Finalizado"}">
                                                %{--<button data-target="modalInfoCancelar" class="btn btn-small modal-trigger red white-text mtBtn10">Cancelar Pedido</button>--}%
                                                    <g:link action="cancelarPedido" controller="cardapio" id="${cardapio.id}"
                                                        class="btn btn-small blue-grey white-text mtBtn10">Cancelar pedido</g:link>
                                            </g:if>



                            </g:if>


                                            <!-- Modal Structure -->
                                            <div id="modalInfoCancelar" class="modal center-align">
                                                <g:form action="cancelarPedido" controller="cardapio">
                                                    <div class="modal-content ">
                                                        <h4 class="red-text">Espere um pouco!!</h4>
                                                        <p class="fontStronger">Voce tem certeza que deseja cancelar seu pedido?</p>
                                                        <p>Antes de cancelar nos diga o motivo:</p>
                                                        <input id="cardapio_id" name="cardapioId" value="${cardapio.id}" type="text" hidden />
                                                        <input id="usuario_id" name="usuario" value="${session?.usuario?.idPessoa}" type="text" hidden />
                                                        <input id="status_id" name="status" value="49" type="text" hidden />
                                                        <div class="input-field">
                                                            <input id="motivo_id" class="text-area validate" name="motivoCancelamento" type="text" required />
                                                            <label for="motivo_id">Motivo:</label>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="#!" class="modal-close waves-effect waves-blue btn light-blue left">Voltar</a>
                                                        <button type="submit" class="waves-effect waves-green btn green fontStronger">Sim! Desejo.</button>
                                                    </div>
                                                </g:form>
                                            </div>
                        </div>
                        </div>

                    </div>
                </div>
             </div>
         </div>


        <div class="col s12 m12 l6">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title font-mediumCard">
                            Registro de mensagens
                            <button data-target="modalRegistro" class="btn btn-small right waves-effect light-blue modal-trigger hide-on-small-only" >Nova mensagem</button>
                            </span>


                            <p class="pt25">
                            <g:if test="${registros != []}" >
                                 <g:each in="${registros}" var="reg">
                                 <div class="">
                                     <span class="right new badge grey" data-badge-caption="${reg?.envio?.format('dd/MM/yyyy HH:mm')}"></span>

                                     <g:if test="${session?.usuario?.perfil == "Administrador"}" >
                                        <span class="fontStronger text-darken-3 text-blue">${reg?.pessoa?.nome}:</span>
                                     </g:if>
                                     <g:else>
                                         <span class="fontStronger text-darken-3 text-grey">${reg?.pessoa?.nome}:</span>
                                    </g:else>
                                      <p class="pb1rem"> ${reg?.mensagem} </p>
                                </div>
                                 </g:each>
                            </g:if>
                              <g:else>
                              <span class="center">Por enquanto, voce não possui mensagens</span>
                            </g:else>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section>
        <!-- Modal Structure -->
        <div id="modalRegistro" class="modal bottom-sheet">
             <div class="modal-content">
                <g:form action="save" controller="registroConversa">
                    <h5 class="center">Envie uma mensagem...</h5>
                        <p>
                            <input name="pessoa" value="${session?.usuario?.idPessoa}" hidden/>
                            <input name="cardapio" value="${cardapio?.id}" hidden/>
                            <div class="input-field">
                                <label for="mensagem_id">Digite algo...</label>
                                <input name="mensagem" type="text" class="text-area" required id="mensagem_id"/>
                            </div>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button class="waves-effect waves-red left modal-close btn btn-small red ml15">Cancelar</button>
                        <button class="waves-effect waves-green btn btn-small green">Enviar</button>
                    </div>
                </g:form>
             </div>
        </div>
    </section>

    <section>
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large pulse light-blue modal-trigger" data-target="modalRegistro">
                <i class="large material-icons">send</i>
            </a>
        </div>
    </section>
</main>
</body>
</html>
