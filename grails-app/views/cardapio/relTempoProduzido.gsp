<%--
  Created by IntelliJ IDEA.
  User: Kallayo
  Date: 04/12/2018
  Time: 13:08
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Relatorio Tempo produzido</title>
</head>

<body>
<main>
    <section class="container center-align mt25">
        <div class="card hoverable">
            <div class="card-content">
                <h2>
                    Pagina em desenvolvimento, aguarde mais um pouco...
                </h2>
                <p>
                    <i class="material-icons large center-align">update</i>
                </p>
            </div>
        </div>
    </section>
</main>
</body>
</html>