<%--
  Created by IntelliJ IDEA.
  User: Kallayo
  Date: 07/11/2018
  Time: 11:00
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Autentica Medico</title>
</head>

<body>

<main>
    <section class="row containerJumpcatIndex">
        <div class="col s12 m12 l6">
            <h5 class="center-align mt2rem mb2rem">Histórico de Pedidos Recentes
                <p class="mt1rem tituloEstiloso">${new Date().format('MMMM')}</p>
            </h5>

            <g:if test="${itemCardapio != []}">
                <ul class="collapsible popout">

                    <g:each in="${itemCardapio}" var="cardapioMedico">
                        <li id="encontre">
                            <div class="collapsible-header">
                                <div class="col s9 m9 l9">
                                    <div class="font-small"><b>Entrega desejavel:</b> ${cardapioMedico?.pedido?.format('dd/MM/yyyy HH:mm')}
                                    </div>
                                    <span class="font-small"><b>Refeição:</b> ${cardapioMedico?.refeicao?.nome}</span>
                                </div>

                                <div class="col s3 m3 l3">
                                    <g:if test="${cardapioMedico?.status?.nome == "Novo Pedido"}">
                                        <span class="new badge blue" data-badge-caption="${cardapioMedico?.status?.nome}">
                                    </g:if>
                                    <g:elseif test="${cardapioMedico?.status?.nome == "Em preparo"}">
                                        <span class="new badge purple" data-badge-caption="${cardapioMedico?.status?.nome}">
                                    </g:elseif>
                                    <g:elseif test="${cardapioMedico?.status?.nome == "Saiu para entrega"}">
                                        <span class="new badge orange" data-badge-caption="${cardapioMedico?.status?.nome}">
                                    </g:elseif>
                                    <g:elseif test="${cardapioMedico?.status?.nome == "Entregue"}">
                                        <span class="new badge green" data-badge-caption="${cardapioMedico?.status?.nome}">
                                    </g:elseif>
                                    <g:elseif test="${cardapioMedico?.status?.nome == "Cancelado"}">
                                        <span class="new badge red" data-badge-caption="${cardapioMedico?.status?.nome}">
                                    </g:elseif>
                                    <g:else>
                                        <span class="new badge" data-badge-caption="${cardapioMedico?.status?.nome}">
                                    </g:else>
                                </span>
                                </div>

                            </div>


                            <div class="collapsible-body fundoCollapse center-align">
                                <span class="mt-25">
                                    <div class="font-small font-bold mb15">
                                        Montagem:
                                    </div>
                                    <g:each in="${cardapioMedico?.item}" var="item">
                                        <g:if test="${item?.categoria?.nome == "Carboidrato"}">
                                            <div class="chipCategoriaItem chipColorCarboidrato">
                                        </g:if>
                                        <g:if test="${item?.categoria?.nome == "Salada"}">
                                            <div class="chipCategoriaItem chipColorSalada">
                                        </g:if>
                                        <g:if test="${item?.categoria?.nome == "Proteína Vegetal"}">
                                            <div class="chipCategoriaItem chipColorProteinaVegetal">
                                        </g:if>
                                        <g:if test="${item?.categoria?.nome == "Proteína Animal"}">
                                            <div class="chipCategoriaItem chipColorProteinaAnimal">
                                        </g:if>
                                        <g:if test="${item?.categoria?.nome == "Adicional"}">
                                            <div class="chipCategoriaItem chipColorAdicional">
                                        </g:if>
                                        <g:if test="${item?.categoria?.nome == "Bebida"}">
                                            <div class="chipCategoriaItem chipColorBebidas">
                                        </g:if>
                                        <g:if test="${item?.categoria?.nome == "Guarnição"}">
                                            <div class="chipCategoriaItem chipColorGuarnicao">
                                        </g:if>
                                        <g:if test="${item?.categoria?.nome == "Lanche"}">
                                            <div class="chipCategoriaItem chipColorBebidas">
                                        </g:if>
                                        ${item?.nome}
                                        </div>

                                    </g:each>
                                </span>
                                <div class="font-small mb15 mt25 left-align">
                                    <b>Observação:</b> ${cardapioMedico?.observacao}
                                </div>
                                <p class="left-align">
                                    %{--action="cancelarPedido" controller="cardapio" id="${cardapioMedico.id}"--}%
                                    <g:if test="${cardapioMedico?.status?.nome != "Cancelado" && cardapioMedico?.status?.nome != "Entregue" && cardapioMedico?.status?.nome != "Pedido Finalizado"}">
                                        <button data-target="modalInfoCancelar" class="btn btn-small modal-trigger red white-text mtBtn10">Cancelar Pedido</button>
                                    </g:if>
                                    <g:link action="show" controller="cardapio" class="btn btn-small blue mtBtn10" id="${cardapioMedico.id}">Detalhes</g:link>
                                </p>

                            </div>
                        </li>
                        <!-- Modal Structure -->
                        <div id="modalInfoCancelar" class="modal center-align">
                            <div class="modal-content ">
                                <h4 class="red-text">Espere um pouco!!</h4>
                                <p class="fontStronger">Voce tem certeza que deseja cancelar seu pedido?</p>
                                <p>Seu pedido após o cancelamento só irá desaparecer da lista, após validação da Cozinha.</p>
                            </div>
                            <div class="modal-footer center mb1rem">
                                <a href="#!" class="modal-close waves-effect waves-green btn grey mr1">Voltar</a>
                                <g:link action="cancelarPedidoMedico" controller="cardapio" id="${cardapioMedico.id}" class="waves-effect waves-green btn green fontStronger mr1">Sim! Desejo.</g:link>
                            </div>
                        </div>
                    </g:each>

                </ul>
            </g:if>
            <g:else>
                <section>
                    <div class="center-align">
                        <div class="card">
                            <div class="card-content">
                                <h6 class="">Voce ainda não possui nenhum pedido... Clique abaixo para pedir algo.</h6>
                                <g:link controller="cardapio" action="create"
                                        class="btn waves-effect waves-light blue-grey hoverable mt25"><i class="material-icons left white-text">local_dining</i>Solicitar refeição</g:link>
                            </div>
                        </div>
                        <div class="fixed-action-btn">
                            <g:link controller="cardapio" action="create" class="btn-floating btn-large cyan darken-1 pulse">
                                <i class="material-icons white-text">local_dining</i>
                            </g:link>
                        </div>
                    </div>
                </section>
            </g:else>

        </div>

        <div class="col s12 m12 l6">
            <h5 class="center-align mt2rem mb2rem">Agenda Cirurgica de ${session?.usuario?.nome}
                <p class="mt1rem tituloEstiloso">${new Date().format('MMMM')}</p>
            </h5>

            <g:if test="${agendas != []}">
                <ul class="collapsible popout" data-collapsible="accordion">

                    <g:each in="${agendas}" var="agenda">
                        <li id="encontre2">
                            <div class="collapsible-header font-bold">
                                <i class="material-icons adjustIconMargin">date_range</i>

                                <div class="col s4 m5 l5">
                                    <span>${agenda?.data_agenda?.format('dd/MM/yyyy')}
                                    </span>
                                </div>

                                <div class="col s8 m7 l7">
                                    <div class="truncate left-align">${agenda?.procedimento?.nome}</div>
                                </div>
                            </div>

                            %{--collapsible-body-custom--}%
                            <div class="collapsible-body fundoCollapse">
                                <ul class="collection">
                                    <li class="collection-item">
                                        <div class="title font-bold col s10">
                                            <span>Procedimento: ${agenda?.procedimento?.nome}</span>
                                        </div>

                                        <div class="secondary-content font-bold black-text col s2 right-align">
                                            <span>${agenda?.nome_sala}</span>
                                        </div>

                                        <div class="col s12">
                                            <p>Paciente: ${agenda?.paciente?.nome}</p>

                                            <p>Convenio: ${agenda?.convenio?.nome}</p>

                                            <p>Cód. atendimento: ${agenda?.cod_atendimento}</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </g:each>
                </ul>
            </g:if>
            <g:else>
                <section>
                    <div class="center-align card">
                        <div class="card-content">
                            <h6>Voce não possui nenhuma cirurgia agendada ate a data atual... Por enquanto!</h6>
                            <p><i class="material-icons green-text iconeGrande mt25">beach_access</i></p>
                        </div>
                    </div>
                </section>
            </g:else>
        </div>
    </section>
</main>
</body>
</html>