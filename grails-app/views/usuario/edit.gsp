<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
    <main class="container">
        <section class="mt25 ml15">
            <div class="row">
                <g:link action="index" class="btn blue">Voltar para lista</g:link>
            </div>
        </section>
        <section>
            <div class="row">
                <div class="col"></div>

                <div class="col s12 m12 l12">
                    <div class="card white darken-1">
                        <div class="card-content black-text">
                            <span class="card-title center">Editar Usuario</span>
                            <g:form resource="${this.usuario}" method="PUT">
                                <g:hiddenField name="version" value="${this.usuario?.version}" />
                                <g:hiddenField name="id" value="${this.usuario?.id}" />
                                <div class="row">
                                    <div class="input-field col s12 m6 l6">
                                        <input placeholder="login" id="nome_id" type="text" name="login" value="${usuario?.login}" class="validate">
                                        <label for="nome_id">login</label>
                                    </div>
                                    <div class="input-field col s12 m6 l6">
                                        <input id="inicio_id" type="password"  value="${usuario?.senha}" name="senha">
                                        <label for="inicio_id">senha</label>
                                    </div>
                                    <div class="input-field col s12 m6 l6">
                                        <g:select name="perfil" from="${perfis}" optionKey="id" optionValue="${{ it.nome }}"
                                                  id="perfil_id"/>
                                        <label for="perfil_id">Perfil</label>
                                    </div>
                                    <div class="input-field col s12 m6 l6">
                                        <input disabled value="${usuario.pessoa.nome}" id="pessoa_id" type="text" class="validate">
                                        <label for="pessia_id">Funcionário</label>
                                    </div>
                                </div>
                                <button class="btn orange" type="submit">Editar</button>
                            </g:form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="fixed-action-btn">
                <a class="btn-floating btn-large  blue darken-1"><i class="large material-icons">apps</i></a>
                <ul>
                    <li><g:link action="create"  class="btn-floating green"><i class="material-icons">add</i></g:link></li>
                    <li><g:link action="index"  class="btn-floating purple"><i class="material-icons">home</i></g:link></li>
                </ul>
            </div>
        </section>
    </main>

    </body>
</html>
