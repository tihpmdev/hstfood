package hpmfood

class Periodo {

    String nome
    String horaInicial
    String horaFinal
    Boolean exclusaoLogica

    static hasMany = [refeicao: Refeicao, cardapio: Cardapio]

    static constraints = {
        horaInicial nullable: false
        horaFinal nullable: false
        exclusaoLogica nullable: true
    }

    String toString(){
        this.nome
    }
}
