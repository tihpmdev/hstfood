package hpmfood

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PatologiaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() {
        Boolean isExcluido = true
        List<Patologia> patologias = Patologia.findAll('''from Patologia where isExcluido != :excluido order by nome''',[ excluido: isExcluido])
        respond patologias : patologias

    }

    def show(Patologia patologia) {
        respond patologia
    }

    def create() {
        respond new Patologia(params)
    }

    @Transactional
    def save(def params) {

        Patologia patologia = new Patologia()

        Boolean notExcluido = false

        patologia.nome = params.nome
        patologia.observacao = params.observacao
        patologia.isExcluido = notExcluido
        patologia.save flush:true
        redirect(action: 'index')

    }

    def edit(Patologia patologia) {
        respond patologia
    }

    def excluirPatologia(def params){

        Patologia patologia = Patologia.get(params.id)
        patologia.isExcluido = true
        patologia.save(flush: true, failOnError: true)
        redirect(action: 'index')

    }

    @Transactional
    def update(def params) {
        Patologia patologia = Patologia.get(params.id)

        patologia.nome = params.nome
        patologia.observacao = params.observacao
        patologia.save flush:true
        redirect(action: 'index')
    }

    @Transactional
    def delete(Patologia patologia) {

        if (patologia == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        patologia.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'patologia.label', default: 'Patologia'), patologia.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'patologia.label', default: 'Patologia'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
