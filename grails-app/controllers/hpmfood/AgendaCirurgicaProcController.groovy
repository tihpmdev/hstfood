package hpmfood

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AgendaCirurgicaProcController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond AgendaCirurgicaProc.list(params), model:[agendaCirurgicaProcCount: AgendaCirurgicaProc.count()]
    }

    def show(AgendaCirurgicaProc agendaCirurgicaProc) {
        respond agendaCirurgicaProc
    }

    def create() {
        respond new AgendaCirurgicaProc(params)
    }

    @Transactional
    def save(AgendaCirurgicaProc agendaCirurgicaProc) {
        if (agendaCirurgicaProc == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (agendaCirurgicaProc.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond agendaCirurgicaProc.errors, view:'create'
            return
        }

        agendaCirurgicaProc.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'agendaCirurgicaProc.label', default: 'AgendaCirurgicaProc'), agendaCirurgicaProc.id])
                redirect agendaCirurgicaProc
            }
            '*' { respond agendaCirurgicaProc, [status: CREATED] }
        }
    }

    def edit(AgendaCirurgicaProc agendaCirurgicaProc) {
        respond agendaCirurgicaProc
    }

    @Transactional
    def update(AgendaCirurgicaProc agendaCirurgicaProc) {
        if (agendaCirurgicaProc == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (agendaCirurgicaProc.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond agendaCirurgicaProc.errors, view:'edit'
            return
        }

        agendaCirurgicaProc.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'agendaCirurgicaProc.label', default: 'AgendaCirurgicaProc'), agendaCirurgicaProc.id])
                redirect agendaCirurgicaProc
            }
            '*'{ respond agendaCirurgicaProc, [status: OK] }
        }
    }

    @Transactional
    def delete(AgendaCirurgicaProc agendaCirurgicaProc) {

        if (agendaCirurgicaProc == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        agendaCirurgicaProc.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'agendaCirurgicaProc.label', default: 'AgendaCirurgicaProc'), agendaCirurgicaProc.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'agendaCirurgicaProc.label', default: 'AgendaCirurgicaProc'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
