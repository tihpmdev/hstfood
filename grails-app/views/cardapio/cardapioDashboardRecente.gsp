<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'cardapio.label', default: 'Cardapio')}"/>
    <title>DashBoard</title>
</head>

<body>

<main class="mt25">
    <section class="container row">

        <h5 class="center-align mt2rem">Pedidos Diário</h5>

        <div class="container col s12 m12 l12 infoBackground">
            <span class="col s12 m12 l12 font-small center-align">Todos os pedidos com data de entrega desejada para hoje, em ordem descrecente</span>
        </div>
    </section>
    <section class="containerJumpcatIndex">
        <div class="row">
            <div class="col s12 m12 l12">
                <ul class="collapsible popout">

                    <g:each in="${itemCardapio}" var="cardapioMedico">
                        <li id="">
                            <g:if test="${cardapioMedico?.status?.nome == "Novo Pedido"}">
                                <div class="collapsible-header statusNovoPedido">
                            </g:if>
                            <g:elseif test="${cardapioMedico?.status?.nome == "Em preparo"}">
                                <div class="collapsible-header statusEmPreparo">
                            </g:elseif>
                            <g:elseif test="${cardapioMedico?.status?.nome == "Saiu para entrega"}">
                                <div class="collapsible-header statusEmMovimento">
                            </g:elseif>
                            <g:elseif test="${cardapioMedico?.status?.nome == "Entregue"}">
                                <div class="collapsible-header statusEntregue">
                            </g:elseif>
                            <g:elseif test="${cardapioMedico?.status?.nome == "Cancelado"}">
                                <div class="collapsible-header statusCancelado">
                            </g:elseif>
                            <g:elseif test="${cardapioMedico?.status?.nome == "Pedido Finalizado"}">
                                <div class="collapsible-header statusFinalizadoPedido">
                            </g:elseif>
                            <g:else>
                                <div class="collapsible-header">
                            </g:else>
                            <div class="col s10 m11 l10">
                                <div class="col s12 m12 l8">
                                    <div class="font-small"><b>Codigo Pedido:</b> ${cardapioMedico?.id}</div>
                                </div>

                                <div class="col s12 m12 l8">
                                    <div class="font-small"><b>Local de recebimento:</b> ${cardapioMedico?.setor?.nome}
                                    </div>
                                </div>

                                <div class="col s12 m12 l8">
                                    <span class="font-small right-align"><b>Refeição:</b> ${cardapioMedico?.refeicao?.nome}
                                    </span>
                                </div>
                            </div>

                            <div class="col s12 m12 l2 jumpCardDashboard">

                                <g:if test="${cardapioMedico?.status?.nome == "Novo Pedido"}">
                                    <span class="new badge blue" data-badge-caption="${cardapioMedico?.status?.nome}"></span>

                                    <p class="mt25">
                                        <span class="new badge blue font-bold" data-badge-caption="Entrega:  ${cardapioMedico?.pedido?.format('dd/MM/yyyy HH:mm')}"></span>
                                    </p>
                                </g:if>
                                <g:elseif test="${cardapioMedico?.status?.nome == "Em preparo"}">
                                    <span class="new badge purple" data-badge-caption="${cardapioMedico?.status?.nome}"></span>

                                    <p class="mt25">
                                        <span class="new badge purple font-bold" data-badge-caption="Entrega:  ${cardapioMedico?.pedido?.format('dd/MM/yyyy HH:mm')}"></span>
                                    </p>
                                </g:elseif>
                                <g:elseif test="${cardapioMedico?.status?.nome == "Saiu para entrega"}">
                                    <span class="new badge orange" data-badge-caption="${cardapioMedico?.status?.nome}"></span>

                                    <p class="mt25">
                                        <span class="new badge orange font-bold" data-badge-caption="Entrega:  ${cardapioMedico?.pedido?.format('dd/MM/yyyy HH:mm')}"></span>
                                    </p>
                                </g:elseif>
                                <g:elseif test="${cardapioMedico?.status?.nome == "Entregue"}">
                                    <span class="new badge green" data-badge-caption="${cardapioMedico?.status?.nome}"></span>

                                    <p class="mt25">
                                        <span class="new badge green font-bold" data-badge-caption="Entrega:  ${cardapioMedico?.pedido?.format('dd/MM/yyyy HH:mm')}"></span>
                                    </p>
                                </g:elseif>
                                <g:elseif test="${cardapioMedico?.status?.nome == "Cancelado"}">
                                    <span class="new badge red" data-badge-caption="${cardapioMedico?.status?.nome}"></span>
                                    <p class="mt25">
                                        <span class="new badge red font-bold" data-badge-caption="Entrega:  ${cardapioMedico?.pedido?.format('dd/MM/yyyy HH:mm')}"></span>
                                    </p>
                                </g:elseif>
                                <g:elseif test="${cardapioMedico?.status?.nome == "Pedido Finalizado"}">
                                    <span class="new badge black" data-badge-caption="${cardapioMedico?.status?.nome}"></span>
                                    <p class="mt25">
                                        <span class="new badge black font-bold" data-badge-caption="Entrega:  ${cardapioMedico?.pedido?.format('dd/MM/yyyy HH:mm')}"></span>
                                    </p>
                                </g:elseif>
                                <g:else>
                                    <span class="new badge" data-badge-caption="${cardapioMedico?.status?.nome}"></span>
                                    <br>
                                    <div>
                                        <span class="new badge font-bold" data-badge-caption="Entrega:  ${cardapioMedico?.pedido?.format('dd/MM/yyyy HH:mm')}"></span>
                                    </div>
                                </g:else>
                            </div>

                        </div>

                            <div class="collapsible-body fundoCollapse">
                                <span class="mt-25">
                                    <div class="font-small"><b>Médico:</b> ${cardapioMedico?.medico?.pessoa?.nome}</div>

                                    <div class="font-small"><b>Data da Solicitação:</b> ${cardapioMedico?.solicitacao?.format('dd/MM/yyyy HH:mm')}
                                    </div>

                                    <div class="font-small font-bold mb15">
                                        Montagem:
                                    </div>
                                    <g:each in="${cardapioMedico?.item}" var="item">
                                        <g:if test="${item?.categoria?.nome == "Carboidrato"}">
                                            <div class="chipCategoriaItem chipColorCarboidrato">
                                        </g:if>
                                        <g:if test="${item?.categoria?.nome == "Salada"}">
                                            <div class="chipCategoriaItem chipColorSalada">
                                        </g:if>
                                        <g:if test="${item?.categoria?.nome == "Proteína Vegetal"}">
                                            <div class="chipCategoriaItem chipColorProteinaVegetal">
                                        </g:if>
                                        <g:if test="${item?.categoria?.nome == "Proteína Animal"}">
                                            <div class="chipCategoriaItem chipColorProteinaAnimal">
                                        </g:if>
                                        <g:if test="${item?.categoria?.nome == "Adicional"}">
                                            <div class="chipCategoriaItem chipColorAdicional">
                                        </g:if>
                                        <g:if test="${item?.categoria?.nome == "Bebida"}">
                                            <div class="chipCategoriaItem chipColorBebidas">
                                        </g:if>
                                        <g:if test="${item?.categoria?.nome == "Guarnição"}">
                                            <div class="chipCategoriaItem chipColorGuarnicao">
                                        </g:if>
                                        <g:if test="${item?.categoria?.nome == "Lanche"}">
                                            <div class="chipCategoriaItem chipColorBebidas">
                                        </g:if>
                                        ${item?.nome}
                                        </div>
                                    </g:each>

                                    <div class="font-small mb15">
                                        <b>Observação:</b> ${cardapioMedico?.observacao}
                                    </div>
                                </span>

                                <p class="">
                                    <g:if test="${cardapioMedico?.status?.nome == "Novo Pedido"}">
                                        <g:link action="iniciarPreparo" controller="cardapio" id="${cardapioMedico.id}"
                                                class="btn btn-small purple white-text mtBtn10">Iniciar Preparo</g:link>
                                    </g:if>
                                    <g:if test="${cardapioMedico?.status?.nome == "Em preparo"}">
                                        <g:link action="encaminharPedidoEstabelecimento" controller="cardapio"
                                                id="${cardapioMedico.id}"
                                                class="btn btn-small orange white-text mtBtn10">Encaminhar para estabelecimento</g:link>
                                    </g:if>
                                    <g:if test="${cardapioMedico?.status?.nome == "Saiu para entrega"}">
                                        <g:link action="entregarPedidoEstabelecimento" controller="cardapio"
                                                id="${cardapioMedico.id}"
                                                class="btn btn-small green white-text mtBtn10">Entregue no estabelecimento</g:link>
                                    </g:if>
                                    <g:if test="${cardapioMedico?.status?.nome == "Cancelado"}">
                                        <g:link action="reabrirPedido" controller="cardapio" id="${cardapioMedico.id}"
                                                class="btn btn-small blue-grey white-text mtBtn10">Reabrir pedido</g:link>
                                    </g:if>
                                    <g:if test="${cardapioMedico?.status?.nome == "Entregue"}">
                                        <g:link action="finalizarPedido" controller="cardapio" id="${cardapioMedico.id}"
                                                class="btn btn-small black white-text mtBtn10">Finalizar Pedido</g:link>
                                    </g:if>
                                    <g:if test="${cardapioMedico?.status?.nome == "Pedido Finalizado"}">
                                        <g:link action="reabrirPedido" controller="cardapio" id="${cardapioMedico.id}"
                                                class="btn btn-small blue-grey white-text mtBtn10">Reabrir pedido</g:link>
                                    </g:if>
                                    <g:if test="${cardapioMedico?.status?.nome != "Cancelado" && cardapioMedico?.status?.nome != "Entregue" && cardapioMedico?.status?.nome != "Pedido Finalizado"}">
                                        <g:link action="cancelarPedido" controller="cardapio" id="${cardapioMedico.id}"
                                                class="btn btn-small red white-text mtBtn10">Cancelar Pedido</g:link>
                                    </g:if>
                                    <g:link action="show" controller="cardapio" class="btn btn-small blue mtBtn10"
                                            id="${cardapioMedico.id}">Detalhes</g:link>
                                </p>
                            </div>
                        </li>
                    </g:each>
                </ul>
            </div>
        </div>
    </section>
</main>
</body>
</html>