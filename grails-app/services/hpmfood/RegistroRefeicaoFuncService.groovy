package hpmfood

import grails.transaction.Transactional

@Transactional
class RegistroRefeicaoFuncService {

    def listDadosPessoa(Long cpf){

        Pessoa funcionarioDados = Pessoa.find('''from Pessoa as pes where pes.cpf= :cpfInfo''', [cpfInfo: cpf])
        return funcionarioDados

    }
}
