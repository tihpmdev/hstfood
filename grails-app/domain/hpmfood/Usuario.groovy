package hpmfood

class Usuario {

    String login
    String senha
    Perfil perfil
    Pessoa pessoa
    String nomeImg
    byte[] fotoPerfil
    Boolean exclusaoLogica

    static constraints = {
        pessoa nullable: false
        exclusaoLogica nullable: true
        nomeImg nullable: true
        fotoPerfil nullable: true
        perfil nullable: true
        login nullable: false
        senha nullable: false
    }

    String toString(){
        this.login
    }
}
