<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'setor.label', default: 'Setor')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>

<main class="container mb15 mt25">
    <section class="">
        <div class="row">
            <g:link controller="setor" action="index" class="btn ">Voltar para lista</g:link>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="col"></div>

            <div class="col s12 m12 l12">
                <div class="card white darken-1">
                <div class="card-content black-text">
                    <span class="card-title center">Cadastrar Setor</span>
                    <g:form controller="setor" action="save">
                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="Nome do setor" id="nome_id" type="text" name="nome"
                                       class="validate">
                                <label for="nome_id">Nome</label>
                            </div>
                            <div class="input-field col s6">
                                <g:select name="estabelecimento" from="${estab}" optionKey="id" optionValue="${{it.nome}}"
                                          id="estabelecimentos_id" class="validate" required="true"
                                          noSelection="['': 'Selecione o estabelecimento']"/>
                                <label for="estabelecimentos_id">Estabelecimento</label>
                            </div>

                        </div>

                        </div>

                        <div class="card-action">
                            <button class="btn btn-block" type="submit">Cadastrar</button>
                        </div>
                    </g:form>
                </div>
            </div>

            <div class="col"></div>
        </div>
    </section>
</main>
</body>
</html>
