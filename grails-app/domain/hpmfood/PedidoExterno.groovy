package hpmfood

class PedidoExterno {

    Pessoa solicitante
    String paciente
    Refeicao refeicao
    Dieta dieta
    Boolean temAcompanhante
    String observacao
    Status status
    Date solicitacao
    Date pedidoEntrega
    Date encaminhamento
    Date entrega
    Date cancelamento
    Date finalizar
    Boolean exclusaoLogica

    static belongsTo = [setor: Setor]
    static hasMany = [patologia : Patologia]

    static constraints = {
        status nullable: false

        patologia nullable: true
        solicitante nullable: false
        paciente nullable: true
        refeicao nullable: false
        dieta nullable: false
        temAcompanhante nullable: false
        observacao nullable: true
        solicitacao nullable: false
        pedidoEntrega nullable: true

        encaminhamento nullable: true
        entrega nullable: true
        cancelamento nullable: true
        exclusaoLogica nullable: true
        finalizar nullable: true
    }
}
