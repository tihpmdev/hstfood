package autenticacao

import hpmfood.AgendaCirurgica
import hpmfood.Cardapio
import hpmfood.Medico
import hpmfood.Pessoa
import hpmfood.RegistroConversa
import hpmfood.RegrasDataService
import hpmfood.Status
import hpmfood.Usuario


class AutenticacaoController {

    RegrasDataService regrasDataService

    def index() {
        if (!session?.usuario) {
            render(view: 'acesso')
        } else {
            retornaAgendaCirurgicaMensal()
            retornaHistoricoMensalCardapio()
        }
    }

    def acesso() {}

    def autenticaMedico(Long login, String senha) {

        Medico medico = Medico.findById(login)

        if (medico) {

            if (medico.id == login && medico.pessoa.cpf == senha) {

                session.usuario = [
                        crm     : medico.id, nome: medico.pessoa.nome,
                        idPessoa: medico.pessoa.id, cpf: medico?.pessoa?.cpf,
                        sexo    : medico?.pessoa?.sexo
                ]

                flash.message = "Olá ${session?.usuario?.nome}, voce está online!"
                redirect(view: 'index')
            } else {
                flash.message = "Usuário ou Senha incorreto."
                render(view: 'acesso')

            }
        } else {
            flash.message = "Usuario não foi encontrado."
            render(view: 'acesso')
        }
    }

    def autenticaAdmin(String login, String senha) {

        login = login?.toLowerCase()
        Usuario usuario = Usuario.findByLogin(login)

        if (usuario) {

            if (usuario.login == login && usuario.senha == senha) {

                session.usuario = [
                        nome    : usuario?.pessoa?.nome,
                        idPessoa: usuario?.pessoa?.id, cpf: usuario?.pessoa?.cpf,
                        sexo    : usuario?.pessoa?.sexo,
                        login   : usuario?.login,
                        perfil  : usuario?.perfil?.nome
                ]

                flash.message = "Olá ${session?.usuario?.nome}, voce está online!"
                redirect(action: 'cardapioDashboard', controller: 'cardapio')
            } else {
                flash.message = "Usuário ou Senha incorreto."
                render(view: 'acesso')

            }
        } else {
            flash.message = "Usuario não foi encontrado."
            render(view: 'acesso')
        }
    }

    def logout() {
        session.invalidate()
        flash.message = "Sua sessão foi encerrada"
        redirect(view: 'acesso')
    }

    void retornaAgendaCirurgicaMensal() {

        List<AgendaCirurgica> agendasCirurgica = []
        String medicoLogado
        Pessoa idMedico

        Calendar primeiroDiaMes = regrasDataService?.retornaPrimeiroDia()
        Calendar ultimoDiaMes = regrasDataService?.retornaUltimoDia()

        Date dataInicio = primeiroDiaMes.getTime()
        Date dataFim = ultimoDiaMes.getTime()
        Date dataAtual = new Date()


        if (session?.usuario) {

            medicoLogado = session.usuario.idPessoa
            Long parseMedico = Long.parseLong(medicoLogado)
            idMedico = Pessoa.findById(parseMedico)

            agendasCirurgica = AgendaCirurgica.withCriteria {
                eq('medico', idMedico)
                and {
                    between('data_agenda', dataAtual.clearTime(), dataFim)
                    isNotNull('procedimento')
                }
                order('data_agenda', 'asc')
            } as List<AgendaCirurgica>

            respond(agendas: agendasCirurgica)

        } else {
            flash.message = "Sua sessão expirou"
            redirect(action: 'logout', controller: 'autenticacao')
        }
    }

    void retornaHistoricoMensalCardapio() {
        if (session?.usuario) {
            Pessoa medicoLogado = Pessoa.findById(session?.usuario?.idPessoa)
            Medico medico = Medico.findByPessoa(medicoLogado)
            Status pedidoFinalizado = Status.findById(50)
            Boolean isExcluido = false
            Calendar mesAnoPrimeiroDia = regrasDataService.retornaPrimeiroDia()
            Calendar mesAnoUltimoDia = regrasDataService.retornaUltimoDia()

            Date dataInicio = mesAnoPrimeiroDia.getTime()
            Date dataFim = mesAnoUltimoDia.getTime()
            Date dataAtual = new Date()

            List<Cardapio> cardapios = Cardapio.executeQuery('''from Cardapio 
                                                           where medico= :medico
                                                           and pedido >= :dataInicio
                                                           and pedido <= :dataFim
                                                           and status != :pedidoFinalizado 
                                                           and exclusaoLogica != :isExcluido 
                                                           order by periodo desc''', [medico          : medico,
                                                                                      dataInicio      : dataAtual,
                                                                                      dataFim         : dataFim,
                                                                                      isExcluido      : isExcluido,
                                                                                      pedidoFinalizado: pedidoFinalizado])

            respond(itemCardapio: cardapios)
        } else {
            redirect(controller: 'autenticacao', action: 'acesso')
        }
    }


}
