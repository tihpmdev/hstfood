package hpmfood

class Setor {

    String nome
    Estabelecimento estabelecimento

    static hasMany=[pedidoExterno: PedidoExterno]

    static constraints = {
        nome nullable: false
        estabelecimento nullable: false
    }

    String toString(){
        this.nome
    }
}
