package hpmfood

class AgendaCirurgicaProc {

    Long id
    Integer qtdProcedimento
    Convenio convenio
    Procedimento procedimento
    AgendaCirurgica agendaCirurgica

    static constraints = {
    }

    static mapping = {
        version false
        table name: 'MV_AGENDA_CIRURGICA_PROC'
        id column: 'NR_NEX_SEQ'
        qtdProcedimento column: 'QT_PROCEDIMENTO'
        procedimento column: 'NR_SEQ_PROC_INTERNO'
        convenio column: 'CD_CONVENIO'
        agendaCirurgica column: 'NR_SEQUENCIA'
    }
}
