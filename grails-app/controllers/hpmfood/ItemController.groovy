package hpmfood

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ItemController {

//    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() {
        List<Item> itens = Item.executeQuery('''from Item order by nome''')
        respond(item: itens)
    }

    def show(Item item) {
        respond item
    }

    def create() {

        List<Refeicao> listRefeicao = Refeicao.executeQuery("from Refeicao order by nome")
        List<Categoria> listCategoria = Categoria.executeQuery("from Categoria order by nome")
        respond (new Item(params), model: [refeicoes: listRefeicao, categorias: listCategoria])
    }

    @Transactional
    def save(def params) {

        String tempoPreparoMin
        String caloriaParams
        Integer parseTempoPrepato = 0
        Integer parseCaloria = 0

        if(params?.tempoPreparo){
            tempoPreparoMin = params?.tempoPreparo
            parseTempoPrepato = Integer.valueOf(tempoPreparoMin)
        }
        if(params?.caloria){
            caloriaParams = params?.caloria
            parseCaloria = Integer.valueOf(caloriaParams)
        }
        if(!params?.disponivel){
            params?.disponivel = false
        }

        Item item = new Item()
        item.nome = params.nome
        item.disponivel = params.disponivel
        item.caloria = parseCaloria
        item.tempoPreparo = parseTempoPrepato
        item.refeicao = Refeicao.findById(params.refeicao)
        item.categoria = Categoria.findById(params.categoria)

        item.save flush:true, failOnError: true

        redirect(view: 'index')
    }

    def edit(Item item) {
        List<Refeicao> listRefeicao = Refeicao.executeQuery("from Refeicao order by nome")
        List<Categoria> listCategoria = Categoria.executeQuery("from Categoria order by nome")
        respond (item, model: [refeicoes: listRefeicao, categorias: listCategoria])
    }

    @Transactional
    def update(def params) {

        Item item = Item.get(params.id)

        String tempoPreparoMin
        String caloriaParams
        Integer parseTempoPrepato = 0
        Integer parseCaloria = 0

        if(params?.tempoPreparo){
            tempoPreparoMin = params?.tempoPreparo
            parseTempoPrepato = Integer.valueOf(tempoPreparoMin)
        }
        if(params?.caloria){
            caloriaParams = params?.caloria
            parseCaloria = Integer.valueOf(caloriaParams)
        }
        if(!params?.disponivel){
            params?.disponivel = false
        }

        item.nome = params?.nome
        item.disponivel = params.disponivel
        item.caloria = parseCaloria
        item.tempoPreparo = parseTempoPrepato
        item.refeicao = Refeicao.findById(params.refeicao)
        item.categoria = Categoria.findById(params.categoria)

        println(item.disponivel)
        println(params.disponivel)

        item.save flush:true, failOnError:true
        flash.message = "O item foi editado!"
        redirect(action: 'index')
    }

    @Transactional
    def delete(Item item) {

        item.delete flush: true, failOnError: true
        flash.message = "O item foi excluido!"
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'item.label', default: 'Item'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
