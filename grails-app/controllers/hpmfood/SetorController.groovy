package hpmfood

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class SetorController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() {
        List<Setor> setores = Setor.executeQuery('''from Setor order by nome''')
        respond setores: setores
    }

    def show(Setor setor) {
        respond setor
    }

    def create() {
        List<Estabelecimento> estabelecimentos = retornaEstabelecimento()
        respond (new Setor(params), model:[estab: estabelecimentos])
    }

    List<Estabelecimento> retornaEstabelecimento() {
        List<Estabelecimento> estabelecimentos = Estabelecimento.findAll('''from Estabelecimento order by nome''')
        return estabelecimentos
    }

    @Transactional
    def save(Setor setor) {
        if (setor == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (setor.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond setor.errors, view:'create'
            return
        }

        setor.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'setor.label', default: 'Setor'), setor.id])
                redirect setor
            }
            '*' { respond setor, [status: CREATED] }
        }
    }

    def edit(Setor setor) {
        respond setor
    }

    @Transactional
    def update(Setor setor) {
        if (setor == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (setor.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond setor.errors, view:'edit'
            return
        }

        setor.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'setor.label', default: 'Setor'), setor.id])
                redirect setor
            }
            '*'{ respond setor, [status: OK] }
        }
    }

    @Transactional
    def delete(Setor setor) {

        if (setor == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        setor.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'setor.label', default: 'Setor'), setor.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'setor.label', default: 'Setor'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
