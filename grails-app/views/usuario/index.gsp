<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<main class="mb15 mt25 container">

    <section class="row">
        <g:link controller="usuario" action="create" class="btn green">Cadastrar Usuarios</g:link>
    </section>

    <section>
        <g:if test="${flash.message}">
            <p class="center">${flash.message}</p>
        </g:if>
        <h5 class="center-align mb15">Lista de Usuários</h5>
        <table class="striped white">
            <thead>
            <th>Nome</th>
            <th>Login</th>
            <th>Perfil</th>
            <th>Ações</th>
            </thead>
            <tbody>
            <g:each in="${usuarios}" var="usuario">
                <tr>
                    <td>${usuario?.pessoa?.nome}</td>
                    <td>${usuario?.login}</td>
                    <td>${usuario?.perfil}</td>
                    <td><g:link class="btn" controller="Usuario" action="edit" id="${usuario.id}">
                        <i class="large material-icons">create</i>
                    </g:link></td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </section>
    <section>
        <div class="fixed-action-btn">
            <g:link action="create" class="btn-floating btn-large green">
                <i class="large material-icons">add</i>
            </g:link>
        </div>
    </section>
</main>

</body>
</html>