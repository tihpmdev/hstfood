<%--
  Created by IntelliJ IDEA.
  User: Kallayo
  Date: 07/11/2018
  Time: 11:00
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Autentica Medico</title>
</head>

<body>
<main class="container">
    <section class="mtHome">
        <g:if test="${flash.message}">
            <div class="infoBackground center-align ">
                <h6 class="text-darken-2">${flash.message}</h6>
            </div>
        </g:if>

        <div class="row">
            <div class="card">
                <div class="card-tabs">
                    <ul class="tabs tabs-fixed-width">
                        <li class="tab"><a href="#medicoTab" class="font-bold active orange-text">Eu sou Médico</a></li>
                        <li class="tab"><a href="#adminTab" class="font-bold deep-orange-text">Eu sou Administrador</a></li>
                    </ul>
                </div>

                <div class="card-content">
                    <div id="adminTab">
                        <div class="row">
                            <div class="col s12 m12 l12 center-align">
                                <g:form class="col s12 m12 l12" method="post" controller="autenticacao" action="autenticaAdmin">
                                    <div class="input-field col s12 m12 l12">
                                        <input id="admin_id" type="text" name="login" class="validate" required>
                                        <label for="admin_id">Usuário</label>
                                    </div>

                                    <div class="input-field col s12 m12 l12">
                                        <input id="senhaAdmin_id" type="password" name="senha" class="validate"
                                               required>
                                        <label for="senhaAdmin_id">Senha</label>
                                    </div>

                                    <div>
                                        <button type='submit'
                                                class="btn btn-bloco waves-effect deep-orange white-text">Login</button>
                                    </div>
                                </g:form>
                            </div>

                        </div>

                    </div>

                    <div id="medicoTab">

                        <div class="row">
                            <div class="col s12 m12 l12 center-align">
                                <g:form class="col s12 m12 l12" method="post" controller="autenticacao"
                                        action="autenticaMedico">

                                    <div class="input-field col s12">
                                        <input id="crm_id" type="number" name="login" class="validate" required>
                                        <label for="crm_id">Digite sua CRM (Apenas numeros)</label>
                                    </div>

                                    <div class="input-field col s12">
                                        <input id="password" type="password" name="senha" class="validate" required>
                                        <label for="password">Digite seu CPF (Apenas numeros)</label>
                                    </div>

                                    <div>
                                        <button type='submit'
                                                class="btn btn-bloco waves-effect orange white-text">Login</button>
                                    </div>
                                </g:form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="fixed-action-btn">
            <g:link controller="registroRefeicaoFunc" action="create" class="btn-floating btn-large white darken-1">
                <i class="large material-icons black-text iconColor iconSize">assignment_ind</i>
            </g:link>
        </div>

    </section>
</main>
</body>
</html>