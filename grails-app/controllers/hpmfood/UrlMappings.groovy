package hpmfood

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

//        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')
        "/acesso"(controller: 'autenticacao', action: 'acesso')
        "/"(controller: 'autenticacao', action: 'index')
    }
}
