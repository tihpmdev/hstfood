package hpmfood

class Convenio {
    Long id
    String nome

    static constraints = {
    }
    static mapping = {
        version false
        table name: 'MV_CONVENIO'
        id column: 'CD_CONVENIO'
        nome column: 'DS_CONVENIO'
    }

    String toString(){
        this.nome
    }
}
