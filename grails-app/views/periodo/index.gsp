<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'periodo.label', default: 'Periodo')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
       <main class="container mb15 mt25">
           <section>
               <table class="striped">
                   <thead>
                   <tr class="">
                       <th>Nome</th>
                       <th>Hora inicial</th>
                       <th>Hora final</th>
                   </tr>
                   </thead>
                   <tbody class="">
                   <g:each in="${periodo}" var="per">
                        <tr>
                            <td>${per.nome}</td>

                           <td>${per.horaInicial}</td>

                           <td>${per.horaFinal}</td>
                       </tr>
                   </g:each>
                   </tbody>
               </table>
           </section>
           <section class="mt25 mb15">
               <g:link controller="periodo" action="create" class="btn">Criar Periodo</g:link>
           </section>
       </main>
    </body>
</html>