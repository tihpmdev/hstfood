<!doctype html>
<html>
<head>
    <title>Page Not Found</title>
    <meta name="layout" content="main">
    <g:if env="development"><asset:stylesheet src="errors.css"/></g:if>
</head>

<body>
<main class="container">
    <section class="center">

        <h2>Erro: Essa pagina não foi encontrada :( </h2>
        <h3 class="red-text">URL: ${request.forwardURI}</h3>
        <h5>Status: 404</h5>
    </section>
</main>
</body>
</html>
