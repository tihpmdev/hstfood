package hpmfood

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ConvenioController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Convenio.list(params), model:[convenioCount: Convenio.count()]
    }

    def show(Convenio convenio) {
        respond convenio
    }

    def create() {
        respond new Convenio(params)
    }

    @Transactional
    def save(Convenio convenio) {
        if (convenio == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (convenio.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond convenio.errors, view:'create'
            return
        }

        convenio.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'convenio.label', default: 'Convenio'), convenio.id])
                redirect convenio
            }
            '*' { respond convenio, [status: CREATED] }
        }
    }

    def edit(Convenio convenio) {
        respond convenio
    }

    @Transactional
    def update(Convenio convenio) {
        if (convenio == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (convenio.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond convenio.errors, view:'edit'
            return
        }

        convenio.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'convenio.label', default: 'Convenio'), convenio.id])
                redirect convenio
            }
            '*'{ respond convenio, [status: OK] }
        }
    }

    @Transactional
    def delete(Convenio convenio) {

        if (convenio == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        convenio.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'convenio.label', default: 'Convenio'), convenio.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'convenio.label', default: 'Convenio'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
