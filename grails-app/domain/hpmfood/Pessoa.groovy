package hpmfood

class Pessoa {
    Long id
    String nome
    Date nascimento
    String sexo
    String telefone
    String nomeSocial
    String cpf
    String isFuncionario
    Date demissao

    static hasMany = [registroRefeicao : RegistroRefeicaoFunc]

    static constraints = {
    }

    static mapping = {
        version false
        table name: 'MV_PESSOA_FISICA'
        id column: 'CD_PESSOA_FISICA'
        nome column: 'NM_PESSOA_FISICA'
        nascimento column: 'DT_NASCIMENTO'
        sexo column: 'IE_SEXO'
        telefone column: 'NR_TELEFONE'
        nomeSocial column: 'NM_SOCIAL'
        cpf column: 'NR_CPF'
        isFuncionario column: 'IE_FUNCIONARIO'
        demissao column: 'DT_DEMISSAO'
    }

    String toString(){
        this.nome
    }
}
