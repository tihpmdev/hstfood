
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'cardapio.label', default: 'Cardapio')}"/>
    <title>Email Cardapio</title>
</head>

<body>
<div class="container">
    <h3 class="display-4">Olá! Você possui um novo Pedido.</h3>
    <h3 class="display-4">O Dr. ${solicitante}, fez uma nova solicitação de refeição às ${solicitacao.format("dd/MM/yyyy HH:mm")}</h3><br>
    <h3 class="display-4">
        <ul>
            <li>
                Codigo: ${codigoPedido}
            </li>
            <li>
                Entrega desejável: ${entregaPedido.format("dd/MM/yyyy HH:mm")}
            </li>
            <li>
                Observação:
                <g:if test="${observacao}">
                    ${observacao}
                </g:if>
                <g:else>
                    Sem observações.
                </g:else>
            </li>
        </ul>
        <ul>
                Item:
                <g:each in="${itemSolicitado}" var="item">
                    <li>
                        <span>${item?.nome}</span>
                    </li>
                </g:each>
        </ul>

    </h3>
    <ul>
        <li>
            Acesso Interno do sistema: <a href="http://10.0.0.16:8089/hpmfood/" target="_blank">HPMFood - Interno</a>
        </li>
        <li>
            Acesso Externo do sistema: <a href="http://177.126.81.87:8089/hpmfood/" target="_blank">HPMFood - Externo</a>
        </li>
    </ul>

</div>

</body><br>
</html>
