<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
    <main class="container mb15">
        <section class="mt25">
            <div class="row">
                <g:link action="index" class="btn blue">Voltar para lista</g:link>
            </div>
        </section>
        <section>
            <div class="row">
                <div class="col"></div>

                <div class="col s12 m12 l12">
                    <div class="card white darken-1">
                        <div class="card-content black-text">
                            <span class="card-title center">Cadastrar Usuario</span>
                            <g:form controller="usuario" action="save">
                                <div class="row">
                                    <input id="exclusaoLogica_id" type="checkbox" name="isAtivo" hidden/>

                                    <div class="input-field col s12 m6 l6">
                                        <input placeholder="login" id="nome_id" type="text" name="login" class="validate">
                                        <label for="nome_id">login</label>
                                    </div>
                                    <div class="input-field col s12 m6 l6">
                                        <input id="inicio_id" type="password" name="senha">
                                        <label for="inicio_id">senha</label>
                                    </div>
                                    <div class="input-field col s12 m6 l6">
                                        <g:select name="perfil" from="${perfis}" optionKey="id" optionValue="${{ it.nome }}"
                                                  id="perfil_id"/>
                                        <label for="perfil_id">Perfil</label>
                                    </div>
                                    <div class="input-field col s12 m6 l6">
                                        <g:select name="pessoa" from="${funcionarios}" optionKey="id" optionValue="${{ it.nome }}"
                                                  id="pessoa_id"/>
                                        <label for="pessoa_id">Funcionário</label>
                                    </div>
                                </div>
                                <button class="btn" type="submit">Cadastrar</button>
                            </g:form>
                        </div>
                        </div>

                        <div class="col"></div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="fixed-action-btn">
                <g:link action="index" class="btn-floating btn-large blue">
                    <i class="large material-icons">home</i>
                </g:link>
            </div>
        </section>
    </main>
    </body>
</html>
