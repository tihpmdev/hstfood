<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'pedidoExterno.label', default: 'PedidoExterno')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<main class="mt25">
    <g:if test="${flash.message}">
        <section class="container center mt1rem">
            <h4 class="red-text font-bold">${flash.message}</h4>
        </section>
    </g:if>
    <section class="container row">

        <h5 class="center-align mt2rem">Painel de Pedidos Paciente</h5>

        <div class="container col s12 m12 l12 infoBackground">
            <span class="col s12 m12 l12 font-small center-align">Ordenados em  recentes solicitados</span>
        </div>
    </section>

    <section class="containerJumpcatIndex">

        <ul class="collapsible popout">
            <g:each in="${pedidos}" var="pedExt">
                <li>
                    <div class="collapsible-header row no-margin-bottom ">

                        <div class="col s9 m4 xl2 font-small">
                            <span class="hide-on-med-and-down font-bold">Local:</span>
                ${pedExt?.setor?.nome}
                <br>
                <span class="hide-on-med-and-down font-bold mt1rem">Data de Entrega: </span>
                <g:if test="${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm') == null}">
                    Sem data de entrega
                </g:if>
                <g:else>
                    ${pedExt?.pedidoEntrega?.format('dd/MM/yyyy HH:mm')}
                </g:else>
                </div>

                <div class="col s12 m4 xl2 font-small">
                    <span class="hide-on-med-and-down font-bold">Paciente:</span>
                    ${pedExt?.paciente}
                </div>

                <div class="col s12 m4 xl2 font-small">
                    <span class="hide-on-med-and-down font-bold">Dieta:</span>
                    ${pedExt?.dieta}
                </div>

                <div class="col s12 m4 xl2 font-small">
                    <span class="hide-on-med-and-down font-bold">Observação:</span>
                    ${pedExt?.observacao}
                </div>

                <div class="col s12 m4 xl2 jumpCardDashboard right-align">
                    <span class="font-bold mr1 hide-on-med-and-down">Acompanhante:</span>
                    <g:if test="${pedExt?.temAcompanhante == true}">
                        <span class="new badge green show-on-small" data-badge-caption="POSSUI ACOMPANHANTE">
                    </g:if>
                    <g:elseif test="${pedExt?.temAcompanhante == false}">
                        <span class="new badge red show-on-small" data-badge-caption="SEM ACOMPANHANTE">
                    </g:elseif>
                </span>
                </div>
                </div>

                <div class="collapsible-body fundoCollapse">
                    <span class="mt-25">
                        <div class="font-small">
                            <div class="col s2 m12 l12">
                                <span class="font-bold">Pedido:</span>
                                ${pedExt?.id}
                            </div>

                            <div class="col s2 m12 l12">
                                <span class="font-bold">Refeição:</span>
                                ${pedExt?.refeicao?.nome}
                            </div>

                            <div class="col s2 m12 l12">
                                <span class="font-bold">Solicitado em:</span>
                                ${pedExt?.solicitacao?.format('dd/MM/yyyy HH:mm')}
                            </div>

                            <div class="col s2 m12 l12">
                                <span class="font-bold">Local:</span>
                                ${pedExt?.setor?.nome}

                            </div>

                            <div class="col s2 m12 l12">
                                Observação:
                                ${pedExt?.observacao}
                            </div>

                            <div class="col s2 m12 l12">
                                <span class="font-bold">Dieta:</span>
                                ${pedExt?.dieta}
                            </div>
                        </div>

                        <div class="">
                            <g:if test="${cardapioMedico?.status?.nome == "Novo Pedido"}">
                                <span>${pedExt?.status?.nome}</span>
                            </g:if>
                            <g:elseif test="${cardapioMedico?.status?.nome == "Em preparo"}">
                                <span>${pedExt?.status?.nome}</span>
                            </g:elseif>
                            <g:elseif test="${cardapioMedico?.status?.nome == "Saiu para entrega"}">
                                <span>${pedExt?.status?.nome}</span>
                            </g:elseif>
                            <g:elseif test="${cardapioMedico?.status?.nome == "Entregue"}">
                                <span>${pedExt?.status?.nome}</span>
                            </g:elseif>

                            <g:elseif test="${cardapioMedico?.status?.nome == "Cancelado"}">
                                <span>${pedExt?.status?.nome}</span>
                            </g:elseif>

                            <g:elseif test="${cardapioMedico?.status?.nome == "Pedido Finalizado"}">
                                <span>${pedExt?.status?.nome}</span>
                            </g:elseif>
                            <g:else>
                                <span>${pedExt?.status?.nome}</span>
                            </g:else>
                    </span>
                </div>
                </span>

                <p class="">
                    <g:if test="${pedExt?.status?.nome == "Novo Pedido"}">
                        <g:link action="iniciarPreparo" controller="pedidoExterno" id="${pedExt.id}"
                                class="btn btn-small purple white-text mtBtn10">Iniciar Preparo</g:link>
                    </g:if>
                    <g:if test="${pedExt?.status?.nome == "Em preparo"}">
                        <g:link action="encaminharPedidoEstabelecimento" controller="pedidoExterno"
                                id="${pedExt.id}"
                                class="btn btn-small orange white-text mtBtn10">Encaminhar para estabelecimento</g:link>
                    </g:if>
                    <g:if test="${pedExt?.status?.nome == "Saiu para entrega"}">
                        <g:link action="entregarPedidoEstabelecimento" controller="pedidoExterno"
                                id="${pedExt.id}"
                                class="btn btn-small green white-text mtBtn10">Entregue no estabelecimento</g:link>
                    </g:if>
                    <g:if test="${pedExt?.status?.nome == "Cancelado"}">
                        <g:link action="reabrirPedido" controller="pedidoExterno" id="${pedExt.id}"
                                class="btn btn-small blue-grey white-text mtBtn10">Reabrir pedido</g:link>
                    </g:if>
                    <g:if test="${pedExt?.status?.nome == "Entregue"}">
                        <g:link action="finalizarPedido" controller="pedidoExterno" id="${pedExt.id}"
                                class="btn btn-small black white-text mtBtn10">Finalizar Pedido</g:link>
                    </g:if>
                    <g:if test="${pedExt?.status?.nome == "Pedido Finalizado"}">
                        <g:link action="reabrirPedido" controller="pedidoExterno" id="${pedExt.id}"
                                class="btn btn-small blue-grey white-text mtBtn10">Reabrir pedido</g:link>
                    </g:if>
                    <g:if test="${pedExt?.status?.nome != "Cancelado" && pedExt?.status?.nome != "Entregue" && pedExt?.status?.nome != "Pedido Finalizado"}">
                        <g:link action="cancelarPedido" controller="pedidoExterno" id="${pedExt.id}"
                                class="btn btn-small red white-text mtBtn10">Cancelar Pedido</g:link>
                    </g:if>
                    <g:if test="${session?.usuario?.perfil == "Admnistrador"}">
                        <g:link action="excluirPedido" controller="pedidoExterno" id="${pedExt.id}"
                                class="btn btn-small font-bold white red-text mtBtn10">Excluir</g:link>
                    </g:if>

                    %{--<g:link action="show" controller="pedidoExterno" class="btn btn-small blue mtBtn10"--}%
                            %{--id="${pedExt.id}">Detalhes</g:link>--}%
                </p>
                </div>
            </li>
            </g:each>
        </ul>
    </section>

</main>
</body>
</html>