package hpmfood

class Cardapio {

    Date pedido
    Date solicitacao
    Date preparo
    Date encaminhamento
    Date entrega
    Date cancelamento
    Date finalizar
    String observacao
    Periodo periodo
    Refeicao refeicao
    Medico medico
    Status status
    Boolean exclusaoLogica
    Setor setor

    static belongsTo = [refeicao: Refeicao, medico: Medico]
    static hasMany = [item: Item, registroConversa: RegistroConversa, logStatus: LogStatus]

    static constraints = {
        solicitacao nullable: true
        preparo nullable: true
        entrega nullable: true
        encaminhamento nullable: true
        entrega nullable: true
        cancelamento nullable: true
        observacao nullable: true
        finalizar nullable: true
        exclusaoLogica nullable: true
        setor nullable: false
        logStatus nullable: true
    }

    String toString(){
        this.id
    }

}
