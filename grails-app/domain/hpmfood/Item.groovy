package hpmfood

class Item {

    String nome
    Integer caloria
    Integer tempoPreparo
    Refeicao refeicao
    Boolean disponivel
    Boolean exclusaoLogica

    static belongsTo = [refeicao: Refeicao, categoria: Categoria]

    static constraints = {
        tempoPreparo nullable: true
        categoria nullable: true
        caloria nullable: true
        exclusaoLogica nullable: true
        disponivel nullable: true
    }

    String toString(){
        this.nome
    }


}