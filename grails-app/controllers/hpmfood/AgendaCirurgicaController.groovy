package hpmfood

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AgendaCirurgicaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    RegrasDataService regrasDataService

    def index() {
        List<AgendaCirurgica> agendasCirurgica = []
        String medicoLogado
        Pessoa idMedico

        Calendar mesAnoPrimeiroDia = regrasDataService?.retornaPrimeiroDia()
        Calendar mesAnoUltimoDia = regrasDataService?.retornaUltimoDia()

        Date dataInicio = mesAnoPrimeiroDia.getTime()
        Date dataFim = mesAnoUltimoDia.getTime()


        if (session?.usuario) {

            medicoLogado = session.usuario.idPessoa
            Long parseMedico = Long.parseLong(medicoLogado)
            idMedico = Pessoa.findById(parseMedico)

            agendasCirurgica = AgendaCirurgica.withCriteria {
                eq('medico', idMedico)
                and {
                    between('data_agenda', dataInicio, dataFim)
                    isNotNull('procedimento')
                }
                order('data_agenda', 'desc')
            } as List<AgendaCirurgica>

            respond(agendas: agendasCirurgica)

        } else {
            flash.message = "Sua sessão expirou"
            redirect(action: 'logout', controller: 'autenticacao')
        }
    }

    void retornaAgendaCirurgicaMensal() {

        List<AgendaCirurgica> agendasCirurgica = []
        String medicoLogado
        Pessoa idMedico

        Calendar mesAnoPrimeiroDia = regrasDataService?.retornaPrimeiroDia()
        Calendar mesAnoUltimoDia = regrasDataService?.retornaUltimoDia()

        Date dataInicio = mesAnoPrimeiroDia.getTime()
        Date dataFim = mesAnoUltimoDia.getTime()

        if (session?.usuario) {

            medicoLogado = session.usuario.idPessoa
            Long parseMedico = Long.parseLong(medicoLogado)
            idMedico = Pessoa.findById(parseMedico)

            agendasCirurgica = AgendaCirurgica.withCriteria {
                eq('medico', idMedico)
                and {
                    between('data_agenda', dataInicio, dataFim)
                    isNotNull('procedimento')
                }
                order('data_agenda', 'asc')
            } as List<AgendaCirurgica>

            respond(agendas: agendasCirurgica)

        } else {
            flash.message = "Sua sessão expirou"
            redirect(action: 'logout', controller: 'autenticacao')
        }
    }

    def show(AgendaCirurgica agendaCirurgica) {
        respond agendaCirurgica
    }

    def create() {
        respond new AgendaCirurgica(params)
    }

    @Transactional
    def save(AgendaCirurgica agendaCirurgica) {
        if (agendaCirurgica == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (agendaCirurgica.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond agendaCirurgica.errors, view:'create'
            return
        }

        agendaCirurgica.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'agendaCirurgica.label', default: 'AgendaCirurgica'), agendaCirurgica.id])
                redirect agendaCirurgica
            }
            '*' { respond agendaCirurgica, [status: CREATED] }
        }
    }

    def edit(AgendaCirurgica agendaCirurgica) {
        respond agendaCirurgica
    }

    @Transactional
    def update(AgendaCirurgica agendaCirurgica) {
        if (agendaCirurgica == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (agendaCirurgica.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond agendaCirurgica.errors, view:'edit'
            return
        }

        agendaCirurgica.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'agendaCirurgica.label', default: 'AgendaCirurgica'), agendaCirurgica.id])
                redirect agendaCirurgica
            }
            '*'{ respond agendaCirurgica, [status: OK] }
        }
    }

    @Transactional
    def delete(AgendaCirurgica agendaCirurgica) {

        if (agendaCirurgica == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        agendaCirurgica.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'agendaCirurgica.label', default: 'AgendaCirurgica'), agendaCirurgica.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'agendaCirurgica.label', default: 'AgendaCirurgica'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
