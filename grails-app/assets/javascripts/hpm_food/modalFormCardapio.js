// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btnCadastro = document.getElementById("btnCadastro");
// var btnCadastro = $('#btnCadastro');

// Get the <span> element that closes the modal
var span = document.getElementById("btnFecharModal_id");

// When the user clicks on <span> (x), close the modal
span.onclick = function clicado(span) {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


var pedidoData = document.getElementById('pedidoData_id');
var pedidoHora = document.getElementById('pedidoHora_id');
var refeicao = document.getElementById('refeicao_id');

btnCadastro.onclick = function () {

    if (pedidoData.value != ""
        && pedidoHora.value != ""
        && refeicao.value != "null"
        && refeicao.value != "") {

        modal.style.display = "block";
    }
    else {
        M.toast({html: 'Verifique se todos os campos foram preenchidos corretamente.'})
        event.preventDefault()
    }
}
