package hpmfood

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MedicoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() {
        List<Medico> medicos = Medico.executeQuery('''select pessoa.nome from Medico order by pessoa.nome asc''')
        respond(nome: medicos)
    }

    def show(Medico medico) {
        respond medico
    }

    def create() {
        respond new Medico(params)
    }

    @Transactional
    def save(Medico medico) {
        if (medico == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (medico.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond medico.errors, view:'create'
            return
        }

        medico.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'medico.label', default: 'Medico'), medico.id])
                redirect medico
            }
            '*' { respond medico, [status: CREATED] }
        }
    }

    def edit(Medico medico) {
        respond medico
    }

    @Transactional
    def update(Medico medico) {
        if (medico == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (medico.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond medico.errors, view:'edit'
            return
        }

        medico.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'medico.label', default: 'Medico'), medico.id])
                redirect medico
            }
            '*'{ respond medico, [status: OK] }
        }
    }

    @Transactional
    def delete(Medico medico) {

        if (medico == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        medico.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'medico.label', default: 'Medico'), medico.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'medico.label', default: 'Medico'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
