<%--
  Created by IntelliJ IDEA.
  User: Administrador
  Date: 18/12/2018
  Time: 11:58
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Relatório de Pedidos</title>
</head>

<body>
<main>
    <section class="container row">
        <h5 class="center-align mt2rem">Relatório de Pedidos</h5>
    </section>
    <section class="containerJumpcat row no-print relatorioAjax">
        <form>
            <div class="card white">
                <div class="card-content black-text">
                    <div>
                        <span class="center card-title">Selecione para buscar.</span>

                        <p>

                        <div class="input-field col s12 m3 l3">
                            <input type="text" class="autocomplete relatorioInputAjax" name="buscaMedico" id="medico_id"
                                   placeholder="Começe a digitar o nome do medico"/>
                            <label for="medico_id">Médico</label>
                        </div>

                        <div class="input-field col s12 m3 l3">
                            <g:select name="buscaSetor" from="${setores}" optionKey="id"
                                      optionValue="${{ it.nome }}"
                                      id="setor_id" noSelection="['': 'Selecione o Local']"/>
                            <label for="setor_id">Local</label>
                        </div>

                        <div class="input-field col s12 m3 l3">
                            <g:select name="buscaRefeicao" from="${refeicoes}" optionKey="id"
                                      optionValue="${{ it.nome }}"
                                      id="refeicao_id" class="validate" noSelection="['': 'Selecione a Refeição']"/>
                            <label for="refeicao_id">Refeição</label>
                        </div>

                        <div class="input-field col s12 m3 l3">
                            <g:select name="buscaStatus" from="${status}" optionKey="id"
                                      optionValue="${{ it.nome }}"
                                      id="status_id" noSelection="['': 'Selecione o Status']"/>
                            <label for="status_id">Status</label>
                        </div>

                        <div class="input-field col s12 m3 l3">
                            <input type="text" onkeyup="mascaraData(this.value, this)"  name="buscaDataEntrega" id="id_entrega"/>
                            <label for="id_entrega">Data da Entrega</label>
                        </div>

                    </p>
                    </div>
                </div>

                <div class="center pb1rem">
                    <button type="submit" class="btn cyan btn-bloco">Buscar</button>
                </div>
            </div>
        </form>
    </section>

    <section class="mt2rem mb2rem containerJumpcat">

        <g:if test="${cardapios == []}">
            <div class="card white">
                <div class="card-content  black-text">
                    <h5 class="center mb2rem">Nenhum registro encontrado...</h5>
                </div>
            </div>
        </g:if>
        <g:else>
            <table class="white black-text centered mb2rem borderTableRel">
                <thead class="borderTableRel">
                <th>Código</th>
                <th>Solicitante</th>
                <th>Entrega</th>
                <th>Refeição</th>
                <th>Local</th>
                <th>Status</th>
                <th class="no-print">Ações</th>
                </thead>
                <tbody>
                <g:each in="${cardapios}" var="cardapio">
                    <tr class="borderTableRel trTable">
                        <td class="borderTableRel">${cardapio.id}</td>
                        <td class="borderTableRel">${cardapio.medico.pessoa.nome}</td>
                        <td class="borderTableRel">${cardapio?.pedido?.format('dd/MM/yyyy HH:mm')}</td>
                        <td class="borderTableRel">${cardapio.refeicao.nome}</td>
                        <td class="borderTableRel">${cardapio.setor.nome}</td>
                        <td class="borderTableRel">${cardapio.status.nome}</td>
                        <td class="no-print"><g:link action="show" controller="cardapio" class="btn btn-small blue mtBtn10"
                                    id="${cardapio.id}">Detalhes</g:link></td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </g:else>
    </section>
</main>

<input hidden id="listMedicoAPI" value="${createLink(controller: 'cardapio', action: 'retornaMedicoAPI')}"/>

<script type="text/javascript">
    function mascaraData(Data, campo) {
        var data = '';
        data = data + Data;
        if (data.length == 2) {
            data = data + '/';
            campo.value = data;
        }
        if (data.length == 5) {
            data = data + '/';
            campo.value = data;
        }
    }
</script>
</body>
</html>