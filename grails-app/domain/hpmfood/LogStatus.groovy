package hpmfood

class LogStatus {

    Status status
    String descricao
    Date insercao
    Pessoa pessoa

    static belongsTo = [cardapio: Cardapio]

    static constraints = {

        cardapio nullable: false
        status nullable: false
        descricao nullable: false
        pessoa nullable: false
    }
}
