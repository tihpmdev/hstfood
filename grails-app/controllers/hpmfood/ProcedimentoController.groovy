package hpmfood

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ProcedimentoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Procedimento.list(params), model:[procedimentoCount: Procedimento.count()]
    }

    def show(Procedimento procedimento) {
        respond procedimento
    }

    def create() {
        respond new Procedimento(params)
    }

    @Transactional
    def save(Procedimento procedimento) {
        if (procedimento == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (procedimento.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond procedimento.errors, view:'create'
            return
        }

        procedimento.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'procedimento.label', default: 'Procedimento'), procedimento.id])
                redirect procedimento
            }
            '*' { respond procedimento, [status: CREATED] }
        }
    }

    def edit(Procedimento procedimento) {
        respond procedimento
    }

    @Transactional
    def update(Procedimento procedimento) {
        if (procedimento == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (procedimento.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond procedimento.errors, view:'edit'
            return
        }

        procedimento.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'procedimento.label', default: 'Procedimento'), procedimento.id])
                redirect procedimento
            }
            '*'{ respond procedimento, [status: OK] }
        }
    }

    @Transactional
    def delete(Procedimento procedimento) {

        if (procedimento == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        procedimento.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'procedimento.label', default: 'Procedimento'), procedimento.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'procedimento.label', default: 'Procedimento'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
