package hpmfood

class Estabelecimento {

    String nome

    static constraints = {
        nome: nullable: false
    }

    String toString(){
        this.nome
    }
}
