package hpmfood

class Refeicao {

    String nome
    Periodo periodo
    Boolean exclusaoLogica

    static belongsTo = [periodo: Periodo]
    static hasMany = [item : Item]
    static constraints = {
        exclusaoLogica nullable: true
    }


    String toString(){
        this.nome
    }
}
