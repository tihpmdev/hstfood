<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'refeicao.label', default: 'Refeicao')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<main class="container mt15">
    <section class="mt2rem">
        <g:form controller="refeicao" action="save">
            <div class="row">
                <div class="input-field col s6">
                    <input id="nome_id" type="text" name="nome" class="validate">
                    <label for="nome_id">Nome</label>
                </div>

                <div class="input-field col s6">
                    <select id="periodo_id" name="periodo">
                        <option value="" disabled selected>Selecione o Periodo</option>
                        <option value="1">Matutino</option>
                        <option value="2">Vespertino</option>
                        <option value="3">Noturno</option>
                    </select>
                    <label for="periodo_id">Periodo</label>
                </div>
            </div>
            <button class="btn" type="submit">Cadastrar</button>
        </g:form>
    </section>
</main>
</body>
</html>
