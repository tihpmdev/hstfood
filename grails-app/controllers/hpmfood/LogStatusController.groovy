package hpmfood

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class LogStatusController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond LogStatus.list(params), model:[logStatusCount: LogStatus.count()]
    }

    def show(LogStatus logStatus) {
        respond logStatus
    }

    def create() {
        respond new LogStatus(params)
    }

    @Transactional
    def save(LogStatus logStatus) {
        if (logStatus == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (logStatus.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond logStatus.errors, view:'create'
            return
        }

        logStatus.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'logStatus.label', default: 'LogStatus'), logStatus.id])
                redirect logStatus
            }
            '*' { respond logStatus, [status: CREATED] }
        }
    }

    def edit(LogStatus logStatus) {
        respond logStatus
    }

    @Transactional
    def update(LogStatus logStatus) {
        if (logStatus == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (logStatus.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond logStatus.errors, view:'edit'
            return
        }

        logStatus.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'logStatus.label', default: 'LogStatus'), logStatus.id])
                redirect logStatus
            }
            '*'{ respond logStatus, [status: OK] }
        }
    }

    @Transactional
    def delete(LogStatus logStatus) {

        if (logStatus == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        logStatus.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'logStatus.label', default: 'LogStatus'), logStatus.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'logStatus.label', default: 'LogStatus'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
