package hpmfood

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class RefeicaoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Refeicao.list(params), model:[refeicaoCount: Refeicao.count()]
    }

    def show(Refeicao refeicao) {
        respond refeicao
    }

    def create() {
        List<Periodo> periodos = Periodo.findAll()
        respond (new Refeicao(params), model: [periodo: periodos])
    }

    @Transactional
    def save(def params) {

        Refeicao refeicao = new Refeicao()
        refeicao.nome = params.nome
        refeicao.periodo = Periodo.findById(params.periodo)

        refeicao.save flush: true, failOnError: true
        redirect(view: 'index')

    }

    def edit(Refeicao refeicao) {
        respond refeicao
    }

    @Transactional
    def update(Refeicao refeicao) {
        if (refeicao == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (refeicao.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond refeicao.errors, view:'edit'
            return
        }

        refeicao.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'refeicao.label', default: 'Refeicao'), refeicao.id])
                redirect refeicao
            }
            '*'{ respond refeicao, [status: OK] }
        }
    }

    @Transactional
    def delete(Refeicao refeicao) {

        if (refeicao == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        refeicao.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'refeicao.label', default: 'Refeicao'), refeicao.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'refeicao.label', default: 'Refeicao'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
