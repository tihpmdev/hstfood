

$('#refeicao_id').on('change', function() {
    // or $(this).val()
    if(this.value == "14") {//Café da Manhã
        $('#lanche').show();
        $('#proteinaanimal').hide();
        $('#proteinavegetal').hide();
        $('#carboidrato').hide();
        $('#guarnicao').hide();
        $('#salada').hide();
        $('#bebida').show();
        $('#bebida').removeClass("l12");
        $('#bebida').addClass("l2");
        $('#adicional').show();
        $('#adicional').removeClass("l12");
        $('#adicional').addClass("l2");
    }
    if(this.value == "4") {//Almoço
        $('#lanche').hide();
        $('#proteinaanimal').show();
        $('#proteinavegetal').show();
        $('#carboidrato').show();
        $('#guarnicao').show();
        $('#salada').show();
        $('#adicional').hide();
        $('#bebida').hide();
    }
    if(this.value == "15") {//Lanche da Tarde
        $('#lanche').show();
        $('#proteinaanimal').hide();
        $('#proteinavegetal').hide();
        $('#carboidrato').hide();
        $('#guarnicao').hide();
        $('#salada').hide();
        $('#bebida').show();
        $('#bebida').removeClass("l12");
        $('#bebida').addClass("l2");
        $('#adicional').show();
        $('#adicional').removeClass("l12");
        $('#adicional').addClass("l2");

    }
    if(this.value == "16") {//Jantar
        $('#lanche').hide();
        $('#proteinaanimal').show();
        $('#proteinavegetal').show();
        $('#carboidrato').show();
        $('#guarnicao').show();
        $('#salada').show();
        $('#adicional').hide();
        $('#bebida').hide();
    }
    if(this.value == "17") {//Lanche da Noite
        $('#lanche').show();
        $('#proteinaanimal').hide();
        $('#proteinavegetal').hide();
        $('#carboidrato').hide();
        $('#guarnicao').hide();
        $('#salada').hide();
        $('#bebida').show();
        $('#bebida').removeClass("l12");
        $('#bebida').addClass("l2");
        $('#adicional').show();
        $('#adicional').removeClass("l12");
        $('#adicional').addClass("l2");
    }
    if(this.value == "238") {//Colação
        $('#lanche').show();
        $('#proteinaanimal').hide();
        $('#proteinavegetal').hide();
        $('#carboidrato').hide();
        $('#guarnicao').hide();
        $('#salada').hide();
        $('#bebida').show();
        $('#bebida').removeClass("l12");
        $('#bebida').addClass("l2");
        $('#adicional').show();
        $('#adicional').removeClass("l12");
        $('#adicional').addClass("l2");
    }
});