package hpmfood

class Perfil {

    String nome
    Boolean exclusaoLogica

    static constraints = {
        exclusaoLogica nullable: true
    }

    String toString(){
        this.nome
    }
}
