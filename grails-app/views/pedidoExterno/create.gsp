<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'pedidoExterno.label', default: 'PedidoExterno')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<main class="mb2rem mt2rem">
    <section class="mt1rem container center">
        <div class="card white">
            <div class="card-content black-text">
                <span class="card-title">Solicitar Pedido Paciente</span>
                <p>
                    <g:form controller="pedidoExterno" action="save">
                    <input hidden name="solicitante" value="${session?.usuario?.idPessoa}"/>

                    <div class="row">

                        <div class="input-field col s12 m12 l4">
                            <label for="pedidoData_id">Data desejável de entrega</label>
                            <input type="text" required class="datepicker" name="pedidoData" id="pedidoData_id"/>
                        </div>

                        <div class="input-field col  s12 m12 l4">
                            <label for="pedidoHora_id">Horário desejável de entrega</label>
                            <input type="text" required class="timepicker" name="pedidoHora" id="pedidoHora_id"/>
                        </div>

                        <div class="input-field col  s12 m3 l4">
                            <g:select name="setor" from="${locais}" optionKey="id" optionValue="${{ it.nome }}"
                                      id="local_id" class="validate" required="true"
                                      noSelection="['': 'Selecione o Local']"/>
                            <label for="local_id">Local</label>
                        </div>

                        <div class="input-field col s12 m10 l10">
                            <label for="paciente_id">Paciente</label>
                            <input type="text" name="paciente" placeholder="Digite o nome do Paciente" id="paciente_id"
                                   required/>
                        </div>

                        <div class="input-field col s12 m2 l2">
                            <label>
                                <input type="checkbox" name="acompanhante" placeholder="Digite o nome do Paciente"
                                       id="acompanhante_id"/>
                                <span class="left font-bold">Acompanhante</span>
                            </label>
                        </div>

                        <div class="input-field col s12 m12 l12" id="patologia_id">

                         <span class="left">Patologias: </span>
                        <g:each in="${patologia}" var="pat">
                            <div class="col s12 m3 l3">
                                <label>
                                    <input id="${pat.id}" type="checkbox" name="patologia"
                                           value="${pat.id}"/>
                                    <span class="font-small-item">${pat.nome}</span>
                                </label>
                            </div>
                        </g:each>

                         </div>
                    </div>

                    <div class="row">
                        <div class="input-field col  s12 m3 l3">
                            <g:select name="refeicao" from="${refeicoes}" optionKey="id" optionValue="${{ it.nome }}"
                                      id="refeicoes_id" class="validate" required="true"
                                      noSelection="['': 'Selecione a Refeição']"/>
                            <label for="refeicoes_id">Refeição</label>
                        </div>

                        <div class="input-field col  s12 m3 l3">
                            <g:select name="dieta" from="${dietas}" optionKey="id" optionValue="${{ it.nome }}"
                                      id="dieta_id" class="validate" required="true"
                                      noSelection="['': 'Selecione a Dieta']"/>
                            <label for="dieta_id">Dieta</label>
                        </div>

                        <div class="input-field col s12 m6 l6">
                            <label for="observacao_id">Observação</label>
                            <textarea class="materialize-textarea" id="observacao_id" name="observacao"
                                      placeholder="Caso desejar digite uma observação"></textarea>
                        </div>
                    </div>
                </p>
            </div>
            <div class="card-action">
                 <button type="submit" class="btn btn-bloco orange">Solicitar Pedido</button>
                    </g:form>
            </div>
        </div>
    </section>
</main>
</body>
</html>
