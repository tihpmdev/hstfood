<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'patologia.label', default: 'Patologia')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<main class="container mb15">
    <section class="mt25">
        <div class="row">
            <g:link action="index" class="btn blue">Voltar para lista</g:link>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="col"></div>

            <div class="col s12 m12 l12">
                <div class="card white darken-1">
                    <div class="card-content black-text">
                        <span class="card-title center">Editar Patologia</span>
                        <g:form resource="${this.patologia}" method="PUT">
                            <g:hiddenField name="version" value="${this.patologia?.version}"/>
                            <g:hiddenField name="id" value="${this.patologia?.id}"/>
                            <div class="row">
                                <div class="input-field col s12 m12 l12">
                                    <input id="nome_id" type="text" name="nome" value="${patologia?.nome}" required class="validate">
                                    <label for="nome_id">Nome</label>
                                </div>
                                <div class="input-field col s12 m12 l12">
                                    <input id="observacao_id" type="text" class="text-area" value="${patologia?.observacao}" name="observacao" >
                                    <label for="observacao_id">Observação</label>
                                </div>

                            <button class="btn" type="submit">Editar</button>
                        </g:form>
                    </div>
                    </div>

                    <div class="col"></div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="fixed-action-btn">
            <g:link action="index" class="btn-floating btn-large blue">
                <i class="large material-icons">home</i>
            </g:link>
        </div>
    </section>
</main>
</body>
</html>
