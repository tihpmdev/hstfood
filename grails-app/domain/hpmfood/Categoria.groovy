package hpmfood

class Categoria {

    String nome
    Boolean exclusaoLogica

    static hasMany = [item: Item]
    static constraints = {
        exclusaoLogica nullable: true
    }

    String toString(){
        this.nome
    }

}
