package hpmfood

import grails.transaction.Transactional

@Transactional
class CategoriaService {

    List retornaItensPorCategoria() {

        Boolean disponivel = true

        Categoria carboidrato = Categoria.findByNome("Carboidrato")
        Categoria proteinaVegetal = Categoria.findByNome("Proteína Vegetal")
        Categoria proteinaAnimal = Categoria.findByNome("Proteína Animal")
        Categoria guarnicao = Categoria.findByNome("Guarnição")
        Categoria salada = Categoria.findByNome("Salada")
        Categoria lanche = Categoria.findByNome("Lanche")
        Categoria adicional = Categoria.findByNome("Adicional")
        Categoria bebida = Categoria.findByNome("Bebida")

        List<Item> itemCarboidrato = Item.executeQuery('''from Item where categoria= :carboidrato and disponivel= :disponivel  order by nome''', [carboidrato: carboidrato, disponivel: disponivel])
        List<Item> itemProteinaVegetal = Item.executeQuery('''from Item where categoria= :proteinaVegetal and disponivel= :disponivel  order by nome''', [proteinaVegetal: proteinaVegetal, disponivel: disponivel])
        List<Item> itemProteinaAnimal = Item.executeQuery('''from Item where categoria= :proteinaAnimal and disponivel= :disponivel  order by nome''', [proteinaAnimal: proteinaAnimal, disponivel: disponivel])
        List<Item> itemGuarnicao = Item.executeQuery('''from Item where categoria= :guarnicao and disponivel= :disponivel  order by nome''', [guarnicao: guarnicao, disponivel: disponivel])
        List<Item> itemSalada = Item.executeQuery('''from Item where categoria= :salada and disponivel= :disponivel  order by nome''', [salada: salada, disponivel: disponivel])
        List<Item> itemLanche = Item.executeQuery('''from Item where categoria= :lanche and disponivel= :disponivel  order by nome''', [lanche: lanche, disponivel: disponivel])
        List<Item> itemAdicional = Item.executeQuery('''from Item where categoria= :adicional and disponivel= :disponivel order by nome''', [adicional: adicional, disponivel: disponivel])
        List<Item> itemBebida = Item.executeQuery('''from Item where categoria= :bebida and disponivel= :disponivel  order by nome''', [bebida: bebida, disponivel: disponivel])
        [itemAdicional, itemLanche, itemCarboidrato, itemGuarnicao, itemProteinaAnimal, itemProteinaVegetal, itemSalada, itemBebida]
    }
}
