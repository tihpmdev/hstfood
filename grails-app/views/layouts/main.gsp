<!doctype html>
<html lang="pt-br" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <title>
        HPM Food
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <asset:stylesheet src="application.css"/>

    <g:layoutHead/>
</head>

<body>

<g:if test="${session?.usuario}">
%{--<nav class="green darken-3" role="navigation">--}%
    <nav class="mainGradient no-print" role="navigation">
        <div class="nav-wrapper container ajusteContainer">
    <g:if test="${session?.usuario?.perfil == "Administrador"}">
        <a class="brand-logo" href="${createLink(action: 'cardapioDashboard', controller: 'cardapio')}">
            HPMFood
        </a>

        <ul class="right hide-on-med-and-down">
            <li>
                %{--<g:link controller="cardapio" action="historicoPedidoCompleto"--}%
                %{--class="btn green lighten-5 btn-small black-text">Historico Completo</g:link>--}%
                <g:link controller="pedidoExterno" action="create"
                        class="btn green lighten-5 btn-small black-text">Pedido Paciente</g:link>
            </li>
            %{--<li><a class="dropdown-trigger" href="#!" data-target="dropdownFood">Relatórios<i--}%
            %{--class="material-icons right">arrow_drop_down</i></a></li>--}%
            <li><a class="dropdown-trigger" href="#!" data-target="dropdownFood2">Pedido Médico<i
                    class="material-icons right">arrow_drop_down</i></a></li>
            <li><a class="dropdown-trigger" href="#!" data-target="dropdownFood4">Pedido Paciente<i
                    class="material-icons right">arrow_drop_down</i></a></li>
            <li><a class="dropdown-trigger" href="#!" data-target="dropdownFood3">Painel de Controle<i
                    class="material-icons right">arrow_drop_down</i></a></li>
            <li><g:link action="logout" class="orange" controller="autenticacao">Sair</g:link></li>
        </ul>

        <ul id="nav-mobile" class="sidenav">
            <li><div class="user-view">
                <div class="background">
                    <asset:image src="meal.jpg"/>
                </div>
                <a href="#user">
                    <asset:image class="circle" src="profile_pic/admin.jpg"/>
                </a>

                <span class="white-text name">${session?.usuario?.nome}</span>
                <span class="white-text email">${session?.usuario?.perfil}</span>

            </div></li>
            <li><a class="subheader">Painel de Cardapios</a></li>
            <li><g:link controller="cardapio" action="cardapioDashboard">Pedidos</g:link></li>
            <li><g:link controller="cardapio" action="cardapioDashboardRecente">Pedidos Recentes</g:link></li>
            <li><g:link controller="cardapio" action="historicoPedidoCompleto">Historico de Pedidos</g:link></li>
            <li><a class="subheader">Painel de Controle</a></li>
            <li><g:link controller="item" action="index">Itens</g:link></li>
            <li><g:link controller="categoria" action="index">Categorias</g:link></li>
            <li><a class="subheader">Pedido Paciente</a></li>
            <li><g:link controller="pedidoExterno"
                        action="dashBoardPedidoExterno">Dashboard de Pedidos Paciente</g:link></li>
            <li><g:link controller="pedidoExterno" action="create">Novo Pedido Paciente</g:link></li>
            <li><div class="divider"></div></li>
            <li><g:link controller="usuario" action="index">Lista de usuários</g:link></li>
            <li><div class="divider"></div></li>
            <li><g:link action="logout" class="red" controller="autenticacao">Sair</g:link></li>

        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
    </nav>

    <!-- Dropdown Relatorios -->
    %{--<ul id="dropdownFood" class="dropdown-content">--}%
    %{--<li><g:link action="relTempoProduzido" controller="cardapio">Tempo de atividade</g:link></li>--}%
    %{--<li><g:link action="relMedicoPedido" controller="cardapio">Pedidos por Médico</g:link></li>--}%
    %{--</ul>--}%
        <!-- Dropdown Cardápio -->
        <ul id="dropdownFood2" class="dropdown-content">
            <li><g:link controller="cardapio" action="cardapioDashboard">Dashboard Pedidos</g:link></li>
            <li><g:link controller="cardapio" action="cardapioDashboardRecente">Pedidos Recentes</g:link></li>
            <li><g:link controller="cardapio" action="historicoPedidoCompleto">Histórico Completo</g:link></li>
            <li class="divider" tabindex="-1"></li>
            <li><g:link controller="cardapio" action="relPedidos">Relatorio Pedidos Médico</g:link></li>

        </ul>
        <!-- Dropdown Pedido Paciente -->
        <ul id="dropdownFood4" class="dropdown-content">
            <li><g:link controller="pedidoExterno"
                        action="dashBoardPedidoExterno">Dashboard Pedido Paciente</g:link></li>
            <li><g:link controller="pedidoExterno" action="historicoExterno">Histórico Completo</g:link></li>
            <li class="divider" tabindex="-1"></li>
            <li><g:link controller="pedidoExterno" action="relPedidosExterno">Rel. Pedido Paciente</g:link></li>
        </ul>
        <!-- Dropdown Painel de controle -->
        <ul id="dropdownFood3" class="dropdown-content">
            <li><g:link controller="item" action="index">Itens</g:link></li>
            <li><g:link controller="categoria" action="index">Categorias/Grupos</g:link></li>
            <li><g:link controller="patologia" action="index">Patologias</g:link></li>
            <li><g:link controller="usuario" action="index">Usuários</g:link></li>
        </ul>
    </g:if>
    <g:else>
        <a class="brand-logo" href="${createLink(action: 'index', controller: 'autenticacao')}">
            HPMFood
        </a>

        <ul class="right hide-on-med-and-down">
            <li>
                <g:link controller="cardapio" action="create"
                        class="btn orange darken-1 black-text fontStronger btn-small">Solicitar Refeição</g:link>
            </li>
            %{--<li><a class="dropdown-trigger" href="#!" data-target="dropdownFood2">Minhas refeições--}%
            %{--<i class="material-icons right">arrow_drop_down</i></a></li>--}%
            %{--<li><g:link controller="cardapio" action="create">Solicitar Refeição</g:link></li>--}%
            <li><g:link controller="cardapio" action="cardapioMedicoDiario">Historico de Pedidos Diário</g:link></li>
            <li><g:link controller="cardapio" action="index">Historico Cardapio</g:link></li>

            <li><g:link controller="agendaCirurgica"
                        action="index">Agenda Cirurgica (${new Date().format('MMMM')})</g:link></li>

            <li><g:link action="logout" controller="autenticacao">Sair</g:link></li>
        </ul>

        <ul id="nav-mobile" class="sidenav">
            <li><div class="user-view">
                <div class="background">
                    <asset:image src="meal.jpg"/>
                </div>
                <a href="#user">
                    <asset:image class="circle" src="profile_pic/medico.png"/>
                </a>

                <a href="#name"><span class="white-text name">${session?.usuario?.nome}</span></a>
                <a href="#email"><span class="white-text email">CRM: ${session?.usuario?.crm}</span></a>
            </div></li>

            <li><a class="subheader">Cardápio</a></li>
            <li><g:link controller="cardapio" action="create">Solicitar Refeição</g:link></li>
            <li><g:link controller="cardapio" action="index">Historico de Pedidos</g:link></li>
            <li><g:link controller="cardapio" action="cardapioMedicoDiario">Historico de Pedidos Recentes</g:link></li>
            <li><a class="subheader">Agenda Cirurgica</a></li>
            <li><g:link controller="agendaCirurgica" action="index">Acessar Historico Completo</g:link></li>
            <li><div class="divider"></div></li>
            <li><a class="subheader">Relatórios</a></li>
            <li><a href="#">Tempo médio entrega</a></li>
            <li><a href="#">Indicadores</a></li>
            <li><div class="divider"></div></li>
            <li><g:link action="logout" controller="autenticacao">Sair</g:link></li>

        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
    </nav>

        <!-- Dropdown Cardápio -->
    %{--<ul id="dropdownFood2" class="dropdown-content">--}%

    %{--</ul>--}%
    </g:else>

</g:if>
<g:else>
    <nav class="no-print">
        <div class="nav-wrapper mainGradient">
            <a href="#" class="brand-logo center">HPM Food</a>
        </div>
    </nav>
</g:else>

<g:layoutBody/>
<footer class="page-footer mainGradient no-print">
    <div class="container">
        <div class="row">
            <div class="grey-text text-lighten-4">

                <div class="col s12 m6 l6 mb15">
                    <h5 class="white-text">HPM Food</h5>

                    <p class="grey-text text-lighten-4">Sistema de gerenciamento e pedidos de Refeições</p>

                    <p><span class="font-bold">Rayssa Pimenta -</span>Nutricionista Responsável / (63) 98413-0551</p>
                </div>

                <div class="col s12 m6 l6">
                    <p>
                        <span class="font-bold">Limite de horários para solicitação de refeições:</span>
                    <ul class="browser-default">
                        <li>Lanches: 40 minutos de antecedência.</li>

                        <li>Almoço até às 10h.</li>

                        <li>Jantar até às 15:00h.</li>
                    </ul>
                 </p>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="footer-copyright">
        <div class="container">
            Versão 1.3.8 © 2018 Todos os direitos reservados.
            <a class="grey-text text-lighten-4 right" href="mailto:tihpmdev@gmail.com?Subject=HPMFood%20-%20Duvidas/Sugestões" target="_top">TI - Hospital Palmas Medical</a>
        </div>
    </div>
</footer>
<asset:javascript src="application.js"/>
<asset:javascript src="hpm_food/modalFormCardapio.js"/>
<asset:javascript src="hpm_food/retornaMedicoList.js"/>
</body>
</html>
