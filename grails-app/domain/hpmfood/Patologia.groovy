package hpmfood

class Patologia {

    String nome
    String observacao
    Boolean isExcluido

    static belongsTo = [pedidoExterno: PedidoExterno]

    static constraints = {
        nome nullable: false
        observacao nullable: true
        pedidoExterno nullable: true
        isExcluido nullable: true
    }

    String toString(){
        this.nome
    }
}
