var medicosURL = $("#listMedicoAPI").val();

$(document).ready(function () {
    $(".relatorioAjax").on("click", function (){
        $.ajax({
            type: 'GET',
            url: medicosURL,
            success: function(response) {
                var medicosArray = response;
                var dataMedicos = {};
                for (var i = 0; i < medicosArray.length; i++) {
                    console.log(medicosArray[i]);
                    dataMedicos[medicosArray[i]] = medicosArray[i].flag; //medicosArray[i].flag or null
                }
                $('input.autocomplete').autocomplete({
                    data: dataMedicos
                });
            }
        });
    });
});
