<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'setor.label', default: 'Setor')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
    <main class="container mb15 mt25">
        <section>
            <table class="striped">
                <thead>
                <tr class="">
                    <th>Nome</th>
                    <th>Estabelecimento</th>
                </tr>
                </thead>
                <tbody class="">
                <g:each in="${setores}" var="per">
                    <tr>
                        <td>${per.nome}</td>

                        <td>${per.estabelecimento.nome}</td>

                    </tr>
                </g:each>
                </tbody>
            </table>
        </section>
        <section class="mt25 mb15">
            <g:link controller="setor" action="create" class="btn">Criar Setor</g:link>
        </section>
    </main>

    </body>
</html>