<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'cardapio.label', default: 'Cardapio')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<main class=" mb15">
    <section class="containerTableRel">
        <div class="card white">
        <div class="card-content black-text">
        <p>
            <g:form controller="cardapio" action="save">

                <h5 class="center-align mb2rem">Olá, ${session?.usuario?.nome}

                <p class="mt1rem font-medium">
                    Faça seu pedido
                </p>

                <div class="container col s12 m12 l12 infoBackground mt2rem">
                    <span class="col s12 m12 l12 font-normal">Primeiro escolha a data e hora que deseja receber sua refeição, em seguida escolha o turno, refeição e finalmente escolha os itens.</span>
                </div>
                <g:if test="${flash.message}">
                    <section class="container center mt1rem">
                        <h6 class="red-text font-bold">${flash.message}</h6>
                    </section>
                </g:if>
                </h5>

                <input hidden id="medico_id" type="text" name="medico" value="${session?.usuario?.idPessoa}"/>
                <input hidden type="text" name="status" value="22"/>

                <div class="container">
                    <div class="row">
                        <div class="input-field col s12 m12 l4">
                            <label for="pedidoData_id">Data desejável do pedido</label>
                            <input type="text" required class="datepicker" name="pedidoData" id="pedidoData_id"/>
                        </div>

                        <div class="input-field col  s12 m12 l4">
                            <label for="pedidoHora_id">Horário desejável do Pedido</label>
                            <input type="text" required class="timepicker" name="pedidoHora" id="pedidoHora_id"/>
                        </div>

                        <div id="inputPeriodo">
                        </div>

                        <div class="input-field col s12 m12 l4">
                            <select id="refeicao_id" class="browser-default selectJumpCatForced"
                                    name="refeicao" required></select>

                        </div>

                        <div class="input-field col s12 m12 l6">
                            <g:select name="setor" from="${setores}" optionKey="id" optionValue="${{ it.nome }}"
                                      id="local_id" class="validate" required="true"
                                      noSelection="['': 'Local de recebimento da refeição']"/>
                            <label for="local_id">Local:</label>
                        </div>

                        <div class="input-field col s12 m12 l6">
                            <label for="observacao_id">Deseja adicionar uma observação?</label>
                            <input type="text" class="text-area" name="observacao" id="observacao_id"/>
                        </div>

                    </div>
                </div>

                <div class="container row">

                    <div class="input-field col s12 m4 l3" id="proteinaanimal">
                        <ul class="collection with-header">
                            <li class="collection-header red darken-4 white-text"><h6>Proteína Animal</h6></li>
                            <g:each in="${itemProteinaAnimal}" var="protAnimal">
                                <li class="collection-item">
                                    <p>
                                        <label class="proteinaAnimalCheck">
                                            <input id="${protAnimal.id}" type="checkbox" name="item"
                                                   value="${protAnimal.id}"/>
                                            <span class="font-small-item">${protAnimal.nome}</span>
                                        </label>
                                    </p>
                                </li>
                            </g:each>
                        </ul>
                    </div>

                    <div class="input-field col s12 m4 l3" id="carboidrato">
                        <ul class="collection with-header">
                            <li class="collection-header grey darken-1 white-text"><h6>Carboidratos</h6></li>
                            <g:each in="${itemCarboidrato}" var="carboidratos">
                                <li class="collection-item">
                                    <p>
                                        <label class="carboidratoCheck">
                                            <input id="${carboidratos.id}" type="checkbox" name="item"
                                                   value="${carboidratos.id}"/>
                                            <span class="font-small-item">${carboidratos.nome}</span>
                                        </label>
                                    </p>
                                </li>
                            </g:each>
                        </ul>
                    </div>

                    <div class="input-field col s12 m4 l3" id="salada">
                        <ul class="collection with-header">
                            <li class="collection-header green darken-4 white-text"><h6>Salada</h6></li>
                            <g:each in="${itemSalada}" var="salada">
                                <li class="collection-item">
                                    <p>
                                        <label class="saladaCheck">
                                            <input id="${salada.id}" type="checkbox" name="item" value="${salada.id}"/>
                                            <span class="font-small-item">${salada.nome}</span>
                                        </label>
                                    </p>
                                </li>
                            </g:each>
                        </ul>
                    </div>

                    <div class="input-field col s12 m4 l3" id="proteinavegetal">
                        <ul class="collection with-header">
                            <li class="collection-header brown white-text"><h6>Proteína Vegetal</h6></li>
                            <g:each in="${itemProteinaVegetal}" var="protVegetal">
                                <li class="collection-item">
                                    <p>
                                        <label class="proteinaVegetalCheck">
                                            <input id="protVegetal.id" type="checkbox" name="item"
                                                   value="${protVegetal.id}"/>
                                            <span class="font-small-item">${protVegetal.nome}</span>
                                        </label>
                                    </p>
                                </li>
                            </g:each>
                        </ul>
                    </div>

                    <div class="input-field col s12 m4 l3" id="guarnicao">
                        <ul class="collection with-header">
                            <li class="collection-header orange accent-4 white-text"><h6>Guarnição</h6></li>
                            <g:each in="${itemGuarnicao}" var="guarnicao">
                                <li class="collection-item">
                                    <p>
                                        <label class="guarnicaoCheck">
                                            <input id="${guarnicao.id}" type="checkbox" name="item"
                                                   value="${guarnicao.id}"/>
                                            <span class="font-small-item">${guarnicao.nome}</span>
                                        </label>
                                    </p>
                                </li>
                            </g:each>
                        </ul>
                    </div>

                    <div class="input-field col s12 m4 l3" id="lanche">
                        <ul class="collection with-header" id="lanche_lista">
                            <li class="collection-header yellow"><h6>Lanches</h6></li>
                            <g:each in="${itemLanche}" var="lanche">
                                <li class="collection-item">
                                    <p>
                                        <label class="lancheCheck">
                                            <input id="${lanche.id}" type="checkbox" name="item" value="${lanche.id}"/>
                                            <span class="font-small-item">${lanche.nome}</span>
                                        </label>
                                    </p>
                                </li>
                            </g:each>
                        </ul>
                    </div>

                    <div class="input-field col s12 m4 l3" id="adicional">
                        <ul class="collection with-header">
                            <li class="collection-header deep-purple darken-3 white-text"><h6>Adicional</h6></li>
                            <g:each in="${itemAdicional}" var="adicional">
                                <li class="collection-item">
                                    <p>
                                        <label class="adicionalCheck">
                                            <input id="${adicional.id}" type="checkbox" name="item"
                                                   value="${adicional.id}"/>
                                            <span class="font-small-item">${adicional.nome}</span>
                                        </label>
                                    </p>
                                </li>
                            </g:each>
                        </ul>
                    </div>

                    <div class="input-field col s12 m4 l3" id="bebida">
                        <ul class="collection with-header">
                            <li class="collection-header pink darken-3 white-text"><h6>Bebidas</h6></li>
                            <g:each in="${itemBebida}" var="bebida">
                                <li class="collection-item">
                                    <p>
                                        <label class="bebidaCheck">
                                            <input id="${bebida.id}" type="checkbox" name="item"
                                                   value="${bebida.id}"/>
                                            <span class="font-small-item">${bebida.nome}</span>
                                        </label>
                                    </p>
                                </li>
                            </g:each>
                        </ul>
                    </div>

                </div>

                <div class="pb1rem center">
                    <button id="btnCadastro" class="btn btn-bloco corBtnSolicitar font-bold" type="submit">Solicitar</button>
                </div>
                </div>
            </g:form>
        </p>
        </div>
    </div>
    </section>

    <section>
        <!-- The Modal -->
        <div id="myModal" class="modalFood">

            <!-- Modal content -->
            <div class="modal-contentFood">
                <span id="btnFecharModal_id" class="close"></span>

                <div class="modal-headerFood">
                    <h2>Aguarde ...</h2>
                </div>

                <div class="modal-bodyFood">
                    <p>Estamos cuidando de tudo para registrar seu Pedido...</p>
                </div>

                <div class="modal-footerFood">
                    <div class="progress">
                        <div class="indeterminate"></div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</main>

<input hidden id="listPeriodo" value="${createLink(controller: 'cardapio', action: 'refeicaoPorPeriodo')}"/>

<script type="text/javascript">

</script>
</body>
</html>
