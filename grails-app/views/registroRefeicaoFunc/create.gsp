<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'registroRefeicaoFunc.label', default: 'RegistroRefeicaoFunc')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body onload="moveRelogio()">
<main class="mt25">
    <g:if test="${flash.message}">
        <section class="container">
            <h6 class="center">${flash.message}</h6>
        </section>
    </g:if>

    <section class="container">
        <div class="card">
            <div class="card-content">
                <div class="card-title center" id="relogio_id">

                </div>
                <g:form method="post" action="save" controller="registroRefeicaoFunc" class="formRegistroRef">
                    <div class="row">
                        <div class="input-field col s6">
                            <input autofocus id="cpfFuncionario" type="text" class="validate" name="cpfDados">
                            <label for="cpfFuncionario">CPF</label>
                        </div>

                        <div class="input-field col s6">
                            <input id="nome_id" type="text">
                            <span class="helper-text" data-error="wrong" data-success="right">Nome</span>
                        </div>

                        <div class="input-field col s6">
                            <input id="telefone_id" type="text">
                            <span class="helper-text" data-error="wrong" data-success="right">Telefone</span>
                        </div>

                        <div class="input-field col s6">
                            <div class="right-align">
                                <button type="button" id="btnSubmit" class="btn green white-text">Registrar Refeição</button>
                            </div>
                        </div>

                        %{--<input id="pessoa_id" type="text" class="validate" name="pessoaId">--}%
                    </div>
                </g:form>
            </div>
        </div>
    </section>
</main>
<input hidden id="listDadosPessoa"
       value="${createLink(controller: 'registroRefeicaoFunc', action: 'retornaPessoaDados')}"/>
</body>
</html>
