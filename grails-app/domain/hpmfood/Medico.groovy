package hpmfood

class Medico {
    Long id
    String ufCRM
    Pessoa pessoa

    static hasMany = [cardapio: Cardapio]

    static constraints = {
    }

    static mapping = {
        version false
        table name: 'MV_MEDICO'
        id column: 'NR_CRM'
        pessoa column: 'CD_PESSOA_FISICA'
        ufCRM column: 'UF_CRM'
    }
    String toString(){
        this.pessoa.nome
    }
}
