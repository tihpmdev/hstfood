<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'item.label', default: 'Item')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<main class="mb15 mt25 container">

    <section class="row">
        <g:link controller="item" action="create" class="btn green">Cadastrar Item</g:link>
    </section>

    <section>
        <g:if test="${flash.message}">
            <p class="center">${flash.message}</p>
        </g:if>
        <h5 class="center-align mb15">Lista de Itens</h5>
        <table class="table white">
            <thead>
            <tr>
                <th>Nome</th>
                <th>Disponivel</th>
                <th class="center">Calorias</th>
                %{--<th class="center">Refeição</th>--}%
                <th class="center">Categoria</th>
                <th class="center" >Tempo de Preparo</th>
                <th  class="center">Ações</th>
            </tr>
            </thead>
            <tbody class="">
            <g:each in="${item}" var="itemParams">
                <tr>
                    <td>${itemParams?.nome}</td>

                    <td>
                        <g:if test="${itemParams?.disponivel == true}">
                            <span class="green-text fontStronger">Disponivel</span>
                        </g:if>
                        <g:else>
                            <span class="red-text fontStronger">Indisponivel</span>
                        </g:else>
                    </td>

                    <td class="center">${itemParams?.caloria}</td>

                    %{--<td class="center">${itemParams?.refeicao?.nome}</td>--}%

                    <td class="center">${itemParams?.categoria?.nome}</td>

                    <td class="center">${itemParams?.tempoPreparo}</td>

                    <td>
                        <g:link class="btn yellow btn-small" controller="item" action="edit" id="${itemParams.id}"><i class="large material-icons">create</i></g:link>
                        <g:link class="btn red btn-small" controller="item" action="delete" id="${itemParams.id}"><i class="large material-icons">delete</i></g:link>
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </section>
    <section>
        <div class="fixed-action-btn">
            <g:link action="create" class="btn-floating btn-large green">
                <i class="large material-icons">add</i>
            </g:link>
        </div>
    </section>
</main>
</body>
</html>