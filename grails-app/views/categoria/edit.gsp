<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'categoria.label', default: 'Categoria')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
    <main class="container">
        <section class="mt25 ml15">
            <div class="row">
                <g:link action="index" class="btn blue">Voltar para lista</g:link>
            </div>
        </section>
        <section>
            <div class="row">
                <div class="col"></div>

                <div class="col s12 m12 l12">
                    <div class="card white darken-1">
                        <div class="card-content black-text">
                            <span class="card-title center">Editar Categoria/Grupo</span>
                            <g:form resource="${this.item}" method="PUT">
                                <g:hiddenField name="version" value="${this.categoria?.version}"/>
                                <g:hiddenField name="id" value="${this.categoria?.id}"/>
                                <div class="row">
                                    <div class="input-field col s12 m12 l8">
                                        <input id="nome_id" type="text" name="nome" value="${categoria?.nome}" required
                                               class="validate">
                                        <label for="nome_id">Nome</label>
                                    </div>
                                </div>
                                <button class="btn orange" type="submit">Editar</button>
                            </g:form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="fixed-action-btn">
                <a class="btn-floating btn-large  blue darken-1"><i class="large material-icons">apps</i></a>
                <ul>
                    <li><g:link action="create"  class="btn-floating green"><i class="material-icons">add</i></g:link></li>
                    <li><g:link action="index"  class="btn-floating purple"><i class="material-icons">home</i></g:link></li>
                </ul>
            </div>
        </section>
    </main>
    </body>
</html>
