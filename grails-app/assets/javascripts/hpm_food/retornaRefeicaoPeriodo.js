//Retorna refeicao por periodo de acordo com o horário

$("#pedidoHora_id").change(function(){

    var horaMinuto = document.getElementById("pedidoHora_id").value.split(":");
    var hora = parseInt(horaMinuto[0]);
    var minuto = parseInt(horaMinuto[1]);


    if(hora >= 6 && (hora <= 12 && minuto <= 59)){

        $("#inputPeriodo").html("<input hidden name='periodo' value='1' placeholder='matutino' id='periodo_id1' />");
        $("#refeicao_id").html(" <option>Selecione a Refeição</option> <option value='238'>Colação</option> <option value='4'>Almoço</option> ");
    }
    if(hora >= 13 && (hora <= 17 && minuto <= 59)){

        $("#inputPeriodo").html("<input hidden name='periodo' value='2' placeholder='vespertino' id='periodo_id2' />");
        $("#refeicao_id").html("<option>Selecione a Refeição</option> <option value='15'>Lanche da Tarde</option>");
    }
    if(hora >= 18 && (hora <= 23 && minuto <= 59)){

        $("#inputPeriodo").html("<input hidden name='periodo' value='3' placeholder='noturno' id='periodo_id3' />");
        $("#refeicao_id").html("<option>Selecione a Refeição</option> <option value='16'>Jantar</option> <option value='17'>Ceia</option>");
    }
    if(hora >= 0 && (hora <= 5 && minuto <= 59)){

        $("#inputPeriodo").html("<input hidden name='periodo' value='3' placeholder='noturno/madrugada' id='periodo_id4' />");
        $("#refeicao_id").html("<option>Selecione a Refeição</option> <option value='16'>Jantar</option> <option value='17'>Ceia</option>");
    }
});