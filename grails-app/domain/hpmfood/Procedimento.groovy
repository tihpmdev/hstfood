package hpmfood

class Procedimento {

    Long id
    String nome

    static constraints = {
    }

    static mapping = {
        version false
        table name: 'MV_PROC_INTERNO'
        id column: 'NR_SEQUENCIA'
        nome column: 'DS_PROC_EXAME'
    }

    String toString(){
        this.nome
    }
}
