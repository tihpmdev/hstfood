package hpmfood

class Dieta {

    String nome

    static constraints = {
        nome nullable: false
    }

    String toString(){
        this.nome
    }
}
